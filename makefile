# Makefile
# Daniel Selmes 2016
# make script for building the kernel

# Tool info (Can be overridden)
TOOL-PREFIX=i686-elf-
AS=nasm
ASFLAGS?=-f elf32
LD=$(TOOL-PREFIX)ld
LDFLAGS?= -ffreestanding -lgcc -g
CC=$(TOOL-PREFIX)gcc
CFLAGS?=-std=c99 -Wall -Wextra -g -O2 -fno-omit-frame-pointer 
CFLAGS:=$(CFLAGS) -ffreestanding

# Definitions of directories
SRCDIR?=src/kernel
ASMDIR?=$(SRCDIR)/asm
CDIR?=$(SRCDIR)/c
OBJDIR?=obj
BINDIR?=bin
GRUBDIR?=grub
INCLUDEDIR?=include/kernel
DOCDIR=doc
SYSROOTDIR?=sysroot

#Set up global includes
CFLAGS:= $(CFLAGS) -I$(INCLUDEDIR)

# Specify here the target device to write to when doing a 'usb' boot device
# WARNING, BE VERY SURE YOU DON'T GET THIS WRONG, OR YOU COULD WRECK ONE OF
# YOUR HARD DRIVES. 
USB_WRITE_TARGET?=/dev/null

# Main recipie, makes the kernel, disk image and HTML documentation
all: iso img html

# Recipie to make just the kernel
kernel: $(BINDIR)/kernel.bin

CFLAGS:=$(CFLAGS) -D COMPILETIME='"$(shell date)"'

# Dependecy lists for the kernel
KERNEL_C_OBJS = $(patsubst $(CDIR)/%.c, $(OBJDIR)/%.o, $(wildcard $(CDIR)/*.c))
KERNEL_ASM_OBJS = $(patsubst $(ASMDIR)/%.asm, $(OBJDIR)/%.o, $(wildcard $(ASMDIR)/*.asm))
KERNEL_DEPENDS = $(KERNEL_C_OBJS) $(KERNEL_ASM_OBJS)

# Kernel compile and link recipie
$(BINDIR)/kernel.bin: $(KERNEL_DEPENDS) $(SRCDIR)/linker.ld $(BINDIR)
	$(CC) $(LDFLAGS) -T $(SRCDIR)/linker.ld -o $(BINDIR)/kernel.bin -nostdlib -lgcc $(KERNEL_DEPENDS)

# Pattern recipie to build core kernel files
$(OBJDIR)/%.o: $(CDIR)/%.c $(OBJDIR)
	$(CC) $(CFLAGS) -c $< -o $@
$(OBJDIR)/%.o: $(ASMDIR)/%.asm $(OBJDIR)
	$(AS) $(ASFLAGS) $< -o $@

# Recipies to create neccesary directories
$(OBJDIR):
	mkdir $(OBJDIR)
$(BINDIR):
	mkdir $(BINDIR)

# Recipie to make an ISO
iso: danosredux.iso
danosredux.iso: $(SYSROOTDIR)
	grub-mkrescue -o danosredux.iso $(SYSROOTDIR) -- --quiet

# Recipie to make a hard disk image
DRIVE_SIZE=64
img: danosredux.img
danosredux.img: $(SYSROOTDIR)
	cp $(GRUBDIR)/fat32.img danosredux.img
	sudo losetup /dev/loop5 danosredux.img -o 1048576
	mkdir -p tmp_mnt
	sudo mount /dev/loop5 tmp_mnt
	sleep 1
	sudo cp -r $(SYSROOTDIR)/* tmp_mnt/
	sync
	sleep 1
	sudo umount tmp_mnt
	rmdir tmp_mnt
	sudo losetup -d /dev/loop5

# Recipie for building most of the system
$(SYSROOTDIR): $(GRUBDIR)/grub.cfg.src $(BINDIR)/kernel.bin
	mkdir -vp $(SYSROOTDIR)
	mkdir -vp $(SYSROOTDIR)/boot/grub
	cp -v $(GRUBDIR)/grub.cfg.src $(SYSROOTDIR)/boot/grub/grub.cfg
	cp -v $(BINDIR)/kernel.bin $(SYSROOTDIR)/boot/danosredux.bin


usb: danosredux.iso
	@echo NOTE: Writing to the USB drive requires root privelages.
	su --command "dd if=$(DISKDIR)/danosredux.iso of=$(USB_WRITE_TARGET)"

# Documentation recipies
DOC_FILES = $(patsubst $(DOCDIR)/%.md, $(DOCDIR)/html/%.html, $(wildcard $(DOCDIR)/*.md))
doc html: $(DOCDIR)/html $(DOC_FILES)
	
$(DOCDIR)/html: 
	mkdir $(DOCDIR)/html
	
$(DOCDIR)/html/%.html: $(DOCDIR)/%.md $(DOCDIR)/html
	markdown < $< | sed -e 's|DATETIME|$(shell date)|g' > $@
	
# Specialised Doc Recipies
	
	
# Recipie to clean the source tree
clean: 
	rm -rfv $(OBJDIR)
	rm -rfv $(BINDIR)
	rm -rfv $(DOCDIR)/html
	rm -rfv $(SYSROOTDIR)
	rm -fv danosredux.iso
	rm -fv danosredux.img

# Specify dummy recipies
.PHONY: kernel clean iso danosredux.iso img danosredux.img html doc all
