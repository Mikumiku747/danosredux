Danosredux 
==========
Daniel Selmes' low level hardware experiment
--------------------------------------------

About
=====

Danosredux is my lame attempt at an operating system kernel. Right now it 
doesn't have a userspace, or even a command line, because I work on parts of 
the OS in a wierd way. I might get around to making some sort of command line
kind of soon, but for now... eh.

User Documentation
==================
So you actually want to install and use this terrible mangled thing? Here are
a few ways to educate yourself on how to use my system. 

- [Building the kernel](building.html "Building the kernel"): How to get the 
tools to build the kernel, and how you can affect the building of the kernel.

Design Documentation
====================
Mainly here because I needed to write down how the memory management system 
worked before I started writing it, but I'll probably write other design
choice stuff here too.

- [Page frame allocation](page_frame_allocator.html "Page frame allocation"):
How the system implements page fram allocation, how it maps new virtual pages
and keeps track of free physical memory. 
- [Input and output system](io_system.html "Input and output system"): How 
the system does user IO, at the kernel level with IO buffers and direct screen
write outputs. 

______
Documentation built DATETIME  
[Main Page](index.html "Main Page")
