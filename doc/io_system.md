Input and output system
=======================
### Daniel Selmes 2016

Introduction
------------
This document outlines the input and output systems at the kernel level. It 
provides a bit of insight into how the kernel does input and output from the 
keyboard and screen (and maybe the system's IO ports, such as serial). 

Input System
============
At the kernel level
-------------------
At the kernel level, the kernel has hardware interrupt service routines to 
store input events, such as keystrokes, into a circular buffer. These raw
interrupt events are then serviced periodically by the kernel as part of its
realtime system maintainence tasks. These serviced input events can then be 
requested by a userland process through kernel service routines. 

The kernel should provide two interfaces to keyboard input, a realtime, active
state of the keyboard and other input hardware, for when input needs to be 
current and checked the state of. There's also a steamed input, where input
like text is pre-formatted and given to the userland program as a steam-like 
interface for text-based or command-line-like programs. 

Note that in both instances, the kernel needs to service its input seperately
from interrupts, and that even the state-based input interface cannot get this
data before it is serviced by the kernel. However, for convenience and 
accuracy, userland programs can request that the kernel get the latest data, 
which they should do if it's critical that the program gets the latest input.

How the kernel serves input sinks
---------------------------------
The system may have more than once source of input at a time, for example, a
program may always read input, regardless of the input focus, or some other 
unique circumstance may decide whether or not a program receives input. 
The kernel should keep a list of currently operating input sinks, with details
on who owns them, what kind of input they are, and whether or not they are to
recieve input when this round of raw inputs is processed, as well as relevant 
information such as the addresses of the program's input sink data structure 
and info on how to operate on that structure. 

Then, when it comes time to service inputs, the kernel would take new input, 
do any generic processing, and then, process the inputs for each program which
is actively receiving input. These structures and information would be setup
when a program requests access to a new input interface. Note that while the
availability of some special sinks may be controlled by the application that 
owns it, the majority will be automatically controlled by the system. 

Text input stream
-----------------
A text input stream produces input in the form of a readable stream of bytes.
When input is received, but not yet read, it is stored in a buffer for your
application to read. It preserves the order the text was entered, and you must
read from the buffer to clear it out. If the buffer is full, input to that 
interface is discarded, and when the input is switched from active to inactive
or back again, it is cleared, so be sure to read from it frequently enough
that you don't lose input. 

Text input streams will still contain a select few special characters, like 
backspace, arrows, and function keys, unless you specify that you want a
purely text stream. In the case of the latter, the input will only be handed 
to you after a newline or end of stream character is recieved. 

Direct input state
------------------
A direct input state will tell you the state of various input devices such as
the keyboard and mouse. You can query any buttons or properties on these 
devices to find out thier state, as well as query an analogue value on devices
like the mouse or a game controller. It essentially consists of reading a giant
table of values, and the input serving system will update it with the state of
various input devices every so often. 

Output systems
==============
Output systems at the kernel level
----------------------------------
Most applications will just need to output text to whatever console device is
running them, so in these cases, the kernel will simply have a call to write a
string of specified length (or until a given terminator, like null character).

On the other hand, some may need to do graphics processing, and this will 
require the system to be in graphical mode. 

______
Documentation built DATETIME  
[Main Page](index.html "Main Page")
