Building the kernel
==================
### Daniel Selmes 2016

Introduction
------------
This document details the process of obtaining the tools and programs to
build the kernel from scratch on a normal development system. It depends
on what you do and don't need to use to run the thing, but I'm going to
say that you need basic GNU devs tools, a cross compiler, and some way of
getting the kernel binary bootable. You can also slightly customise the way
the kernel is built too, which is useful for tailoring it to your needs 
(albiet only slightly). 

Booting the kernel
------------------
The kernel needs some kind of bootloader, and more specifically, it needs to 
be multiboot compliant, which is why I reccomend GRUB, it makes that easy. 
There are several ways you can get the operating system running, but the most
obvious one is to make a bootable CD. You can also just build the kernel 
binary itself, and then figure out how to load in a kernel (maybe you have
GRUB or something on your PC which you use to boot your OS already.) And
thirdly, there is also a target, which extends upon the CD one, which writes a
bootable ISO image to a device (be careful with this one, it uses the `dd`
command which is kind of scary).

Required tools
--------------
The tools you need vary depending on what you want to do, but most importantly
*you need a working cross compiler*. It should be targeted at `i686-elf-none`
and should be freestanding, that is, not using any of the host system's
libraries. There's an excellect 
[OSdev Tutorial on building a cross compiler](http://wiki.osdev.org/GCC_Cross-Compiler)
That's the main requirement, but there are others too, depending on what you 
want to build:

### Kernel (The core kernel binary)

- `i686-elf-none Cross` Compiler (`GCC`, `ld`, and others. See above.)
- `nasm`
- `make` (Preferrably GNU Make)
- Some basic system tools like `date`

### ISO Image (Additional tools for bootable ISO)

- `grub-mkrescue`
- `sed`

### USB/Block Device Burning

- `dd`
- `su`

### Documentation

- `markdown` which converts Markdown docfiles to HTML
- `sed` for text control

Building the kernel
-------------------
Once you have all the relevant tools installed, change into the top directory 
for the project:

- To get just the kernel binary, run `make` or `make kernel`. 
- To maek a bootable ISO image, run `make iso`.
- To write the bootable ISO to a device, run `make usb` and specify the device
to use in the environment under the variable `USB_WRITE_TARGET`, 
eg. `USB_WRITE_TARGET=/dev/sdb make usb`
- To build the documentation, run either `make doc` or `make html`, it doesn't
really matter...

You can clean out the project by running `make clean`, but beware that this 
might delete something you didn't want deleted. It completely removed the
`bin`, `obj`, and any disk-related build files.

### Options
There are several options which affect different parts of the build stage, you
can specify them on the command line or in the environment. The most useful 
and common ones are:

- `TOOL_PREFIX`, `AS`, `ASFLAGS`, `CC`, `CFLAGS`  - These are all tools and 
tool options, you should be careful with what you do here or it simply won't 
compile. `TOOL_PREFIX` is your toolchain prefix (probably `i686-elf-none`) and
the rest are tool names appended to that prefix (nasm doesn't use the prefix.)
- `SRCDIR`, `ASMDIR`, `OBJDIR`, `DOCDIR`, `BINDIR`, `DISKDIR`, `INCLUDEDIR` - 
the names of all the directories you'll be using. If you get the source from 
multiple spots or something, this is what you should be changing. 

### Build outputs
The kernel binary is located in `bin/kernel.bin`, this is what you should use
analyse the binary, since the one in the ISO maybe be stripped/optimized. 
The bootable ISO is located in `disk/danosredux.iso` and can be burned or ran
from a virtual machine. The HTML docs can be found in `doc/html` after building 
the docs. There's also markdown files in just the `doc` directory if you want to 
read them for some specific reason (The HTML files are generated from them).

______
Documentation built DATETIME  
[Main Page](index.html "Main Page")
