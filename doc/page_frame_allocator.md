Page Frame Allocator Specification
==================================
### Daniel Selmes 2016

Introduction
------------
This document details how new pages are obtained, for a given process (or the
system itself), and how they are selected, removed and returned to the free
page pool. 

The short version is, I keep track of chunks of free pages in the system using
an array of know free chunks. The list contains some entries, each with a position
and length that denote a set of free pages. 

Theory
------
As mentioned above, the system keeps track of free areas of contiguous memory,
using an array of free chunks. Each entry in the list has a position, where
the range of free pages starts, and a size, which has the number of pages that
are in that area. When a process asks for some memory, it eats into one of 
those free  sections. When a process returns pages, it tells the system the 
physical address the pages were in and a new entry in the list is created to 
fill any of the gaps. 

Once pages are allocated, they're immediately mapped into the process, and the
process' memory map keeps track of where they are. This means it's super hard,
almost impossible, for the memory system to know where a given physical page
is mapped to, but that's an uncommon operation, and we CAN perform a slow and
costly search if absolutely necessary. 

The other, more pressing problem is that the memory area will become fragmented
which is bad  because we can only have a set amount of items in the array at 
any given time. This means that the system should probably periodically run 
some defragmenting process after it returns a certain amount of pages, or if 
the number of spaces in the array runs out. 

Implementation
--------------

## Free pages array
The Free pages array consists of a series of entries detailing the starting
position and the number of pages in the list. The entries are 2 Double-Words
(4 bytes) long, each containing a Double Word for the position and lenth. 
There are also some special bits as well inside each of those areas. 

<table>
<tr> <td> Free area entry layout </td> </tr>
<tr> <td> Offset (Byte:Bits-Bits) </td> <td> Name </td> <td> Description </td></tr>
<tr> <td> 0x00:31-12 </td> <td> Position </td> <td> Top 20 bits of the first page in the entry. Essentially the page number. </td> </tr>
<tr> <td> 0x00:11-01 </td> <td> Reserved </td> <td> Reserved for free memory area attributes. </td> </tr>
<tr> <td> 0x00:00-00 </td> <td> Present Bit </td> <td> Indicates that this entry in the linked list is not occupied yet. </td> </tr>
<tr> <td> 0x04:31-00 </td> <td> Length </td> <td> The length of the region, the bottom 12 bits should be zero since it should be page aligned. </td> </tr>
</table>

### Present Bit
This bit indicates whether the entry in the free pages array is full. When you 
create an entry, you look in the array until you find one with it cleared, and
then you set it and fill the entry. When removing an entry (such as merging an
entry with an adjacent one when defragging) you clear the bit when the entry
is no longer valid. 

Operations
----------
## Getting some pages
Essentially, the OS, at some very low level, will have a function to map some
pages into a process' address space. It will take a destination, where it 
wants them mapped to, and a length, the amount of pages to map. Now that you
have context, you follow these steps. 

1. Find a chunk of free memory. The first one will do, but maybe if you're 
efficient you know where to find one which can fit the area you're asking for.
2. Take note of the length of that chunk and begin mapping pages into the 
current process' memory map. This is where the *where* part of what the client
wants come into play, since that's where you'll begin mapping them to. 
3. When you've allocated enough pages, adjust the start of the chunk by the
number of pages you allocated and shrink the length by that amount too. 
4. If you run out, clear this entry, Now, find another chunk to use, and rinse
and repeat. 

That should get a bunch of pages mapped properly. Remember, this is just for 
the system to ask for blank pages, if it wants something *on* those pages it 
will put it there itself. 

## Returning pages to the pool. 
This is slightly trickier, since we have to look up the pages' physical 
addresses and return them to the correct spot. We should also keep track of 
how many non-contiguous areas we return, so that the defragmenter can be run 
when the chunks are sufficiently messy. In this case, we're given the virtual 
address of the page and the number of pages being returned. 

1. Get the first page to be returned, and look up its physical address. This is
in the current process' memory map. You may have to traverse some of the paging
structures to look it up. 
2. Once you have its physical address, free the page and create a new entry in
the first available slot. You might want to keep a track of the last entry that
was removed so that you can put a new slot in there. Increment the counter for 
the number of chunks returned.
3. We have an entry, now move on to the next page that needs to be cleared, and
if it is adjacent to the one we just cleared, instead of finding a new entry, 
just expand the current one slightly. 
4. If it's not adjacent, create a new chunk in the way described in step 2. 
5. Check according to the defrag algorithm if a defrag is due, and if so, 
schedule or perform one. 

## Defragmenting the list.
Essentially, we're just checking to see if there's any chunks which are 
physically next to each other, but are marked as seperate chunks. To do this,
we go through the list, and basically check each chunk against every other one
and if it's "Next" to any, we clear it out. 

1. Select a chunk, pressumably we'll go through the list in order. 
2. Now, find the end of this chunk, and search through all other chunks to see
if any are after it and adjacent. 
3. If you find one that's after it and adjacent, merge it with the current one
and recalculate the end. Make sure to mark it as no longer present.
4. Now, find the beginning of this chunk, and search through all other chunks
to see if they are adjacent and before it. You need to calculate the end of each
chunk you search. 
5. If there are any matches, merge and mark them as clear. 

This is... probably pretty slow, it might be worse than O(n^2), but, it makes
everything else easier. Something you could probably do to help is sort all
the chunks, since then you never need to check behind. An alternate algorithm
would be to search the list for the lowest one, then the next lowest one, then
the next lowest one, and append them, but that still requires O(n^2) searches. 

Notes
-----

## Lock and protect the free chunks map. 
Since this is such an important data structure, you should clear interrupts and
take any other necessary steps to make sure that it's not edited while any
memory allocation is taking place. Otherwise, you'll have a *really* bad time.

## Speed of obtaining and freeing blocks
Depending on how fragmented either the free chunks map is or the fragmentation
of the chunks when a large area was allocated, you might run into the problem
of the algorithms needing to go through lots of blocks to get the right amount
of memory, which makes page frame allocation slow.
______
Documentation built DATETIME  
[Main Page](index.html "Main Page")
