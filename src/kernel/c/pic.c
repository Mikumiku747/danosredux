/* 
 * pic.c
 * Daniel Selmes 2015
 * Functions for controlling and configuring the 8259A Programmable interrupt
 * controller (PIC).
*/

/* Include my own header. */
#include <pic.h>

/* This will re-map the PICS to thier new offsets. Also masks all, so we don't
 * get any unwelcome interrupts until we're ready for them. */
void PIC_remap()
{
	/* First, we initialise the PICS. We'll be setting the mode too. */
	outb(PORT_PIC1_COMMAND, PIC_INIT + PIC_INIT_MODE);
	outb(PORT_PIC2_COMMAND, PIC_INIT + PIC_INIT_MODE);
	/* Next, we send the PICS thier new offsets. */
	outb(PORT_PIC1_DATA, PIC1_OFFSET);
	outb(PORT_PIC2_DATA, PIC2_OFFSET);
	/* Next, tell the master where the slave is and the slave what it is. */
	outb(PORT_PIC1_DATA, 0b00000100); /* Slave on line 2. */
	outb(PORT_PIC2_DATA, 0b00000010); /* This is number 2 in line. */
	/* Finally, tell them what mode to be in. */
	outb(PORT_PIC1_DATA, PIC_MODE_8086);
	outb(PORT_PIC2_DATA, PIC_MODE_8086);
	/* We also mask ALL interrupts on both PICS, with the exception of 
	 * line 2 on the master, since it is the slave's line. */
	outb(PORT_PIC1_DATA, 0b11111011);
	outb(PORT_PIC2_DATA, 0b11111111);
}

/* Lets us mask an IO line. */
void PIC_mask_line(int line)
{
	uint16_t port;
	if (line < 8) { /* Choose the right PIC. */
		port = PORT_PIC1_DATA;
	} else {
		port = PORT_PIC2_DATA;
		line -= 8;
	}
	int mask = inb(port);
	mask |= (0x01 << line); /* Enable the desired bit. */
	outb(port, mask);
}

/* Lets us unmask a line. */
void PIC_unmask_line(int line)
{
	uint16_t port;
	if (line < 8) { /* Choose the right PIC. */
		port = PORT_PIC1_DATA;
	} else {
		port = PORT_PIC2_DATA;
		line -= 8;
	}
	int mask = inb(port); 
	mask &= ~(0x01 << line); /* Disable the desired bit. */
	outb(port, mask);
}
