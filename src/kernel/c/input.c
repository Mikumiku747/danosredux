/*
 * input.c
 * Daniel Selmes 2016
 * Manages input for the operating system by servicing the scancode queue.
 * We also keep track of keyboard state.
*/

/* Include my own header. */
#include <input.h>

/* Some variables to keep track of common keyboard state settings. There
 * are also some macros to make easier use of them. */
int kbd_shiftLeft = 0;
int kbd_shiftRight = 0;
int kbd_ctrlLeft = 0;
int kbd_ctrlRight = 0;
int kbd_altLeft = 0;
int kbd_altRight = 0;
int kbd_capslock = 0;
int kbd_numlock = 0;
int kbd_scrllock = 0;

/* This table maps any printable scancodes to thier ascii characters so that
 * they can be printed. Most of it is blanks. */
char scancodes_map_printchars[224] = {
  /*0/8     1/9     2/A     3/B     4/C     5/D     6/E     7/F*/
  '\x00', '\x00', '1',    '2',    '3',    '4',    '5',    '6',    /* 0x00 */
  '7',    '8',    '9',    '0',    '-',    '=',    '\x00', '\x00', 
  
  'Q',    'W',    'E',    'R',    'T',    'Y',    'U',    'I',    /* 0x10 */ 
  'O',    'P',    '[',    ']',    '\n',   '\x00', 'A',    'S',    
  
  'D',    'F',    'G',    'H',    'J',    'K',    'L',    ';',    /* 0x20 */
  '\"',   '~',    '\x00', '\\',   'Z',    'X',    'C',    'V',    
  
  'B',    'N',    'M',    ',',    '.',    '/',    '\x00', '*',    /* 0x30 */
  '\x00', ' ',    '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', 
  
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '7',    /* 0x40 */
  '8',    '9',    '-',    '4',    '5',    '6',    '+',    '1', 
  
  '2',    '3',    '0',    '.',    '\x00', '\x00', '\x00', '\x00', /* 0x50 */
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', 
  
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', /* 0x60 */
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', 
  
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', /* 0x70 */
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', 
  
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', /* 0x80 */
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', 
  
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', /* 0x90 */
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', 
  
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', /* 0xA0 */
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', 
  
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', /* 0xB0 */
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', 
  
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', /* 0xC0 */
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', 
  
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', /* 0xD0 */
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00' 
};

/* This function causes the OS to service input. For now, it just prints 
 * characters to the screen so we can see the keyboard worked. In future, this
 * should feed the characters to the appropriate input feed of the current 
 * program, be it userspace or kernelspace. */

void service_keyboard()
{
	uint8_t scancode;
	char printc;
	/* First things first, in order to avoid the scancode queue getting
	 * modified while in use, we'll mask out keyboard interrupts. */
	PIC_mask_line(1);
	/* We'll iterate through scancodes in the scancode queue until none
	 * are left to handle. We just print them. */
	while (scancode_buffer_out != scancode_buffer_in) {
		/* Get the first part of the scancode. */
		scancode = scancode_buffer_area[scancode_buffer_out];
		switch (scancode) {
		  case 0xE0:
			/* It's extended, and they aren't ever printable, so 
			 * we ignore them in this case. Adjust the out slot to
			 * the start of the next scancode and move on. */
			scancode_buffer_out = (scancode_buffer_out + 2) % SCANCODE_BUFFER_SIZE;
			break;
		  case 0xE1:
		  case 0xE2:
		  case 0xE3:
			/* These are also shortened versions of extended keys,
			 * and we also have no use for them. Adjust the out
			 * slot and move on. */
			scancode_buffer_out = (scancode_buffer_out + 1) % SCANCODE_BUFFER_SIZE;
			break;
		  default:
			/* These might be valid scancodes, let's try to map
			 * them to a printable character using a special print
			 * mapping table. */
			printc = scancodes_map_printchars[scancode];
			/* If it's not a zero, print it out. */
			if (printc != '\x00') {
				terminal_putc(printc);
			}
			/* Adjust the out slot ready to read the next character. */
			scancode_buffer_out = (scancode_buffer_out + 1) % SCANCODE_BUFFER_SIZE;
			break;
		}
	}
	/* We can upadte the cursor position since we're done printing for now.. */
	terminal_update_cursor();
	/* We're now safe to do keyboard interrupts again... */
	PIC_unmask_line(1);
}
