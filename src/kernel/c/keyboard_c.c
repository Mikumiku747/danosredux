/* 
 * keyboard_c.c
 * Daniel Selmes 2016
 * Keyboard related functions and data structures. 
*/

/* Include our own header. */
#include <keyboard.h>

/* Initialises the keyboard buffer. */
void scancode_buffer_init()
{
	scancode_buffer_in = 0;
	scancode_buffer_out = 0;
}

/*
 * Handles a keyboard interrupt. Details as follows:
 * - Read the byte from the keyboard. It will be a set 1 scancode. 
 * - Put it in the keyboard events buffer. This is a FIFO buffer of stuff we
 *  have gotten from the keyboard. The OS services that at its own pace.
 * - Tell the PIC we've handled the interrupt. 
*/
void keyboard_handler()
{
	/* Check how many bytes there are in the scancode and read them out as neccesary. */
	uint8_t scancode1 = inb(PORT_KBD_IO);
	uint8_t scancode2;
	switch (scancode1) {
	  case 0xE0:
		/* It's more than 1 byte. */
		/* Check if it's 2-byte or several-byte. */
		scancode2 = inb(PORT_KBD_IO);
		switch (scancode2) {
		  case 0x2A:
			/* It's a printscreen pressed. */
			/* Read out the rest of the printscreen. */
			inb(PORT_KBD_IO);
			inb(PORT_KBD_IO);
			/* We store this as an 0xE2 because we're lazy. */
			/* Check if there's room to put in our scancode. */
			if ((scancode_buffer_in + 1) % SCANCODE_BUFFER_SIZE == scancode_buffer_out) {
				/* We have room, store the 0xE2. */
				scancode_buffer_area[scancode_buffer_in] = 0xE2;
				scancode_buffer_in = (scancode_buffer_in + 1) % SCANCODE_BUFFER_SIZE;
			}
			break;
		  case 0xB7:
			/* It's a printscreen released. */
			/* Read out the rest of the printscreen. */
			inb(PORT_KBD_IO);
			inb(PORT_KBD_IO);
			/* We store this as an 0xE3 because we're lazy. */
			/* Check if there's room to put in our scancode. */
			if ((scancode_buffer_in + 1) % SCANCODE_BUFFER_SIZE == scancode_buffer_out) {
				/* We have room, store the 0xE2. */
				scancode_buffer_area[scancode_buffer_in] = 0xE3;
				scancode_buffer_in = (scancode_buffer_in + 1) % SCANCODE_BUFFER_SIZE;
			}
			break;
		  default:
			/* It's a 2-byte scancode. */
			/* Check if there's room for TWO more entries in the scancode buffer. */
			if (((scancode_buffer_in + 1) % SCANCODE_BUFFER_SIZE != scancode_buffer_out) && 
			    ((scancode_buffer_in + 2) % SCANCODE_BUFFER_SIZE != scancode_buffer_out)) {
				/* There's space, write the first byte. */
				scancode_buffer_area[scancode_buffer_in] = scancode1;
				/* Increment the "in" slot, accounting for wrapping. */
				scancode_buffer_in = (scancode_buffer_in + 1) % SCANCODE_BUFFER_SIZE;
				/* Store the second byte. */
				scancode_buffer_area[scancode_buffer_in] = scancode2;
				/* Increment the "in" slot again. */
				scancode_buffer_in = (scancode_buffer_in + 1) % SCANCODE_BUFFER_SIZE;
			}
		}
		break;
	  case 0xE1:
		/* It's a pause! */
		/* Read out the whole damn thing... */
		inb(PORT_KBD_IO);
		inb(PORT_KBD_IO);
		inb(PORT_KBD_IO);
		inb(PORT_KBD_IO);
		/* We store this as an 0xE3 because we're lazy. */
			/* Check if there's room to put in our scancode. */
			if ((scancode_buffer_in + 1) % SCANCODE_BUFFER_SIZE == scancode_buffer_out) {
				/* We have room, store the 0xE2. */
				scancode_buffer_area[scancode_buffer_in] = 0xE3;
				scancode_buffer_in = (scancode_buffer_in + 1) % SCANCODE_BUFFER_SIZE;
			}
		break;
	  default:
		/* It's a normal 1-byte scancode. */
		/* Check if there's any space in the scancode buffer. */
		if ((scancode_buffer_in + 1) % SCANCODE_BUFFER_SIZE == scancode_buffer_out) {
			/* There's no space. Just do nothing really.... */
		} else {
			/* There's space, save it in. */
			scancode_buffer_area[scancode_buffer_in] = scancode1;
			/* Increment the IN position, with wrapping. */
			scancode_buffer_in = (scancode_buffer_in + 1) % SCANCODE_BUFFER_SIZE;
		}
		break;
	}
	/* NOW, we have to reset the PICs. We always know this will be 
	 * triggered by hardware interrupt 1, so we don't need to find out 
	 * which PIC to clear. */
	/* Tell the master PIC we're done. */
	outb(PORT_PIC1_COMMAND, PIC_EOI);
	/* All done! */
	return;
}
