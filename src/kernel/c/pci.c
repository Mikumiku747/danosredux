/*
 * pci.c
 * Daniel Selmes 2016
 * Provides functions for finding and interacting with devices in the PCI
 * configuration space. 
*/

/* Includes */
#include <pci.h>
#include <stdint.h>
#include <io.h>
#include <vga.h>

/* If this value is defined, the PCI_enumerate_devices function will be lazy
 * and simply check all devices on all busses, which is wastefully slow. The
 * reason this exists is that so I can make a simple enumeration function now
 * and a more complex, but efficient, function later. */
#define PCI_LAZY_ENUMERATE


/* PCI_conf_set_addr
 * Uses the PCI address port to set the configuration space address to be
 * read. Use in conjunction with PCI_conf_read and PCI_conf_write. Note that
 * the register must be 32-bit aligned (the bottom 2 bits are ignored.) */
void PCI_conf_set_addr(uint8_t bus, uint8_t device, uint8_t function, uint8_t reg)
{
	uint32_t addr  = ((uint32_t)0x80000000) | \
			 (((uint32_t)bus << 16) & 0x00FF0000) | \
			 (((uint32_t)device << 11) & 0x0000F800) | \
			 (((uint32_t)function << 8) & 0x00000700) | \
			 (((uint32_t)reg) & 0x000000FC) | \
			 ((uint32_t)0x80000000);
	outl(PCI_CONF_ADDR, addr);
	return;
}

/* PCI_conf_get_addr
 * Use this to get the current PCI configuration space register, you'll need
 * to decode it yourself. */
uint32_t PCI_conf_get_addr()
{
	return inl(PCI_CONF_ADDR);
}

/* PCI_conf_read
 * Reads a word from PCI configuration space, use PCI_conf_set_addr to set
 * which PCI configuration register to read from. */
uint32_t PCI_conf_read()
{
	return inl(PCI_CONF_DATA);
}

/* PCI_conf_write
 * writes a word to PCI configuration space, use PCI_conf_set_addr to set 
 * which PCI configuration register to write to. */
void PCI_conf_write(uint32_t data)
{
	outl(PCI_CONF_DATA, data);
	return;
}

/* PCI_conf_read_data
 * Reads out data from a specific PCI configuration register, by first setting
 * the register to be read from and then performing a read. */
uint32_t PCI_conf_read_data(uint8_t bus, uint8_t device, uint8_t function, uint8_t reg)
{
	PCI_conf_set_addr(bus, device, function, reg);
	return PCI_conf_read();
}

/* PCI_conf_write_data
 * Writes out data to a specific PCI configuration register by first setting
 * the register to be written to and then performing a write. */
void PCI_conf_write_data(uint8_t bus, uint8_t device, uint8_t function, uint8_t reg, uint32_t data)
{
	PCI_conf_set_addr(bus, device, function, reg);
	PCI_conf_write(data);
	return;
}

/* PCI_enumerate_devices
* Searches through the PCI Configuration space for all devices which exist. 
* device_list is an array of device info structures to put the found devices
* in. limit is the maximum amount of devices to put into the array, after 
* which function will return.  A device is considered valid if its vendor ID
* is not 0xFFFF. Returns number of devices found. */
uint32_t PCI_enumerate_devices(struct PCI_basic_device_info *device_list, uint32_t limit)
{
#ifdef PCI_LAZY_ENUMERATE
	uint32_t current_device = 0;
	uint32_t ID_info;
	uint32_t header_type;
	uint8_t highest_function;
	for (uint8_t bus = 0; bus < 0xFF; bus++) {
		for (uint8_t device = 0; device < 0x1F; device++) {
			ID_info = PCI_conf_read_data(bus, device, 0, 0);
			if ((ID_info & 0x0000FFFF) != 0x0000FFFF) {
				/* Check to see if there are more functions for this device. */
				header_type = PCI_conf_read_data(bus, device, 0, 0x0C);
				if ((header_type && 0x00800000) != 0) {
					highest_function = 0x07;
				} else {
					highest_function = 0x01;
				}
				for (int function = 0; function < highest_function; function++) {
					ID_info = PCI_conf_read_data(bus, device, function, 0);
						if ((ID_info & 0x0000FFFF) != 0x0000FFFF) { 
						/* This is a valid device, so save it to the array. */
						device_list[current_device].bus = bus;
						device_list[current_device].device = device;
						device_list[current_device].function = function;
						device_list[current_device].vendor_ID = (uint16_t)ID_info;
						device_list[current_device].device_ID = (uint16_t)((ID_info & 0xFFFF0000) >> 16);
						uint32_t class_info = PCI_conf_read_data(bus, device, function, 0x8);
						device_list[current_device].class = (uint8_t)((class_info & 0xFF000000) >> 24);
						device_list[current_device].subclass = (uint8_t)((class_info & 0x00FF0000) >> 16);
						/* Increment current device and check for end. */
						current_device++;
						if (current_device >= limit) {
							return current_device;
						}
					}
				}
			}
		}
	}
	return current_device;
#else
	/* The efficient version of the function hasn't been coded yet, so
	 * make sure that the lazy version is used for now. */
#endif
}

/* PCI_dump_devices
 * Dumps a list of the PCI devices currently on the PCI bus. */
void PCI_dump_devices(struct PCI_basic_device_info *pci_device_list, uint32_t limit)
{
	uint32_t no_of_devices = PCI_enumerate_devices(pci_device_list, limit);
	terminal_puts("Detected 0x");
	terminal_put_hexdw(no_of_devices);
	terminal_puts(" PCI Devices:\n");
	for (uint8_t i = 0; i < no_of_devices; i++) {
		terminal_puts("    Bus: 0x");
		terminal_put_hexb((uint32_t)pci_device_list[i].bus);
		terminal_puts(" Device: 0x");
		terminal_put_hexb((uint32_t)pci_device_list[i].device);
		terminal_puts(" Function: 0x");
		terminal_put_hexb((uint32_t)pci_device_list[i].function);
		terminal_puts(" Vendor ID: 0x");
		terminal_put_hexw((uint32_t)pci_device_list[i].vendor_ID);
		terminal_puts(" Device ID: 0x");
		terminal_put_hexw((uint32_t)pci_device_list[i].device_ID);
		terminal_puts("\n    Class: 0x");
		terminal_put_hexb((uint32_t)pci_device_list[i].class);
		terminal_puts(" Subclass: 0x");
		terminal_put_hexb((uint32_t)pci_device_list[i].subclass);
		terminal_puts(" - ");
		switch (pci_device_list[i].class) {
			case 1:
				terminal_puts("Mass Storage Controller");
				break;
			case 2:
				terminal_puts("Network Controller");
				break;
			case 3:
				terminal_puts("Display Controller");
				break;
			case 4:
				terminal_puts("Multimedia Controller");
				break;
			case 5:
				terminal_puts("Memory Controller");
				break;
			case 6:
				terminal_puts("Bridge Device");
				break;
			case 7:
				terminal_puts("Simple Communication Controller");
				break;
			case 8:
				terminal_puts("Base System Prehipherals");
				break;
			case 9:
				terminal_puts("Input Device");
				break;
			case 10:
				terminal_puts("Docking Station");
				break;
			case 11:
				terminal_puts("Processor");
				break;
			case 12:
				terminal_puts("Serial Bus Controller");
				break;
			case 13:
				terminal_puts("Wireless Controller");
				break;
			case 14:
				terminal_puts("Intelligent I/O Controller");
				break;
			case 15:
				terminal_puts("Satellite Communication Controller");
				break;
			case 16:
				terminal_puts("Encryption/Decryption Controller");
				break;
			case 17:
				terminal_puts("Data Aquisition / Signal Processing Controller");
				break;
			default:
				terminal_puts("Other");
				break;
		}
		terminal_puts("\n");
	}
}

/* PCI_global_device_list and PCI_global_device_count
 * A list of all the devices on the PCI bus, so that it doesn't need to be
 * searched by every single thing that uses PCI. Use PCI_global_enumerate
 * to update it. PCI_global_device_count contains the number of devices in 
 * the list. */
struct PCI_basic_device_info PCI_global_device_list[PCI_MAX_GLOBAL_DEVICES];

/* PCI_global_enumerate
 * Enumerates the global device list, and notes the number of devices in the 
 * list as well. */
void PCI_global_enumerate()
{
	PCI_global_device_count = PCI_enumerate_devices(PCI_global_device_list, PCI_MAX_GLOBAL_DEVICES);
}

