/* 
 * idt_c.c
 * Daniel Selmes 2016
 * C routines related to the creation, editing, and loading of the interrupt
 * descriptor table (IDT). 
*/

/* Include our own header. */
#include <idt.h>

/* This fills out an IDT entry for you to make your life easier. */
void idt_encode_entry(int slot, void (*offset)(), uint16_t selector, uint8_t flags)
{
	interrupt_descriptor_table[slot].offset_low = (uint16_t)((uint32_t)offset); /* High and low part of the offset. */
	interrupt_descriptor_table[slot].offset_high = (uint16_t)((uint32_t)offset >> 16);
	interrupt_descriptor_table[slot].reserved = 0x00; /* The reserved section must be zero. */
	interrupt_descriptor_table[slot].flags = flags; /* The flags define the type and properties. */
	interrupt_descriptor_table[slot].selector = selector; /* Selector in the GDT. */
}

/* This fills out a bunch of entries in the IDT for us. */
void idt_fill()
{
	 for (int intno = 0; intno < 256; intno++) {
		idt_encode_entry(intno, 0, 0x08, IDT_FLAGS_UNUSED);
	 }
}