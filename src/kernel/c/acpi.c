/*
 * acpi.c
 * Daniel Selmes 2016
 * Functions relating to general ACPI features such as ACPI Tables
 * and power management functions.
*/

/* Includes */
#include <stdint.h>
#include <acpi.h>
#include <mem.h>

/* We need something to compare the pointer signature against. */
char *target_sig = "RSD PTR ";

/* find_rsd_ptr
 * Searches for the RSDT pointer structure in a given memory area. It looks
 * for the string "RSD PTR " on a 16 byte boundary, so you should supply it a 
 * 16 byte aligned address. Also validates any entries it finds to ensure they
 * pass at least the rsd_ptr checksum. */

struct rsd_ptr *find_rsd_ptr(uint32_t start, uint32_t end)
{
	uint8_t match;
	
	/* First, we'll search through and try and find the pointer. */
	for (uint32_t offset = start; offset < end; offset = offset + 16) {
		/* Check to see if we got a match. */
		match = memcmp((char *)offset, target_sig, 8);
		if (match == 0) {
			/* Now we need to verify the pointer. */
			uint8_t check = 0;
			uint8_t *bytes = (uint8_t *)offset;
			for (int i = 0; i < 20; i++) {
				check = check + bytes[i];
			}
			if (check == 0) {
				return (struct rsd_ptr *)offset;
			}
		}
	}
	return 0;
}
