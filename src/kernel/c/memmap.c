/*
 * memmap.c
 * Daniel Selmes 2016
 * Provides a map of available memory regions and thier size, and manipulates
 * them in preperation for the memory management system. 
*/

/* Include our own header. */
#include <memmap.h>

/* Includes */
#include <page_allocator.h>

/* 
 * print_bios_memmap
 * Debug call to show the memory map the bootloader gave us. It pretty much
 * just does a GRUB 'lsmmap' command from inside the OS. We use this to help
 * debug when setting up the memory manager.
*/
void print_bios_memmap(uint32_t length, uint32_t start_addr)
{
	terminal_puts("Bootloader Memory Map:\n");
// 	__asm__("xchgw %ebx, %ebx"); /* Breakpoint. */
	uint32_t location = 0;
	uint32_t *current_entry_offset;
	uint32_t entry_size, entry_start_l, entry_length_l, entry_type;
// 	uint32_t entry_start_h, entry_length_h;
	while (location < length) {
		/* We get the entry to work with. */
		current_entry_offset = (uint32_t *)(start_addr + location);
		/* The first 4 bytes are the length of this entry. */
		entry_size = current_entry_offset[0];
		/* Next 8 are the start of the block. */
		entry_start_l = current_entry_offset[1];
// 		entry_start_h = current_entry_offset[2];
		/* Next 8 are the entry length. */
		entry_length_l = current_entry_offset[3];
// 		entry_length_h = current_entry_offset[4];
		/* The rest are the type. */
		entry_type = current_entry_offset[5];
		/* Print out the details of this entry. */
// 		__asm__("xchgw %bx, %bx"); /* Breakpoint. */
		terminal_puts("Start: 0x");
// 		terminal_put_hexdw(entry_start_h);
		terminal_put_hexdw(entry_start_l);
		terminal_puts(" Length: 0x");
// 		terminal_put_hexdw(entry_length_h);
		terminal_put_hexdw(entry_length_l);
		switch (entry_type) {
			case 1:
				terminal_puts(" Type: Available");
				break;
			case 2:
				terminal_puts(" Type: Reserved");
				break;
			default:
				terminal_puts(" Type: Other (Unavailable)");
				break;
		}
		terminal_puts(" \n");
		/* Now, we move to the next entry by adding the size of this one. */
		location += entry_size + 4; /* The entra 4 is to account for the size itself. */
	}
}

void print_bios_memmap_raw(uint32_t length, uint32_t start_addr)
{
	uint32_t *bytes = (uint32_t *)start_addr;
	terminal_puts("Raw memory map data: \n");
	while (bytes < (uint32_t *)(start_addr + length)) {
		terminal_puts("0x");
		terminal_put_hexdw(bytes[0]);
		terminal_puts(" 0x");
		terminal_put_hexdw(bytes[1]);
		terminal_puts(" 0x");
		terminal_put_hexdw(bytes[2]);
		terminal_puts(" 0x");
		terminal_put_hexdw(bytes[3]);
		terminal_puts(" 0x");
		terminal_put_hexdw(bytes[4]);
		terminal_puts(" 0x");
		terminal_put_hexdw(bytes[5]);
		bytes += 6;
		terminal_puts("\n");
	}
}

void print_page_pool(uint32_t start_addr)
{
	uint32_t *words = (uint32_t *)start_addr;
	terminal_puts("Page pool entries:\n");
	for (int entryno = 0; entryno < 8; entryno++)
	{
		if (words[entryno*2] & 0x00000001) {
			terminal_puts("Start Page: 0x");
			terminal_put_hexdw(words[entryno*2] & 0xFFFFF000);
			terminal_puts(" Length: 0x");
			terminal_put_hexdw(words[(entryno*2)+1]);
			terminal_puts(" Pages.\n");
		} else {
			terminal_puts("Empty Entry.\n");
		}
	}
}

void initialise_free_chunks_array(struct free_page_chunk free_page_array[], 
				  uint32_t memmap_start, 
				  uint32_t memmap_length,
				  uint32_t kernel_start,
				  uint32_t kernel_end
				 )
{
	/* First, let's go through the memory map and make a new chunk for all
	 * the entries above the 1M mark. */
	int current_chunk = 0;
	uint32_t memmap_location = 0;
	uint32_t *current_entry_offset;
	uint32_t entry_size, entry_start_l, entry_length_l, entry_type;
// 	uint32_t entry_start_h, entry_length_h;
	while (memmap_location < memmap_length) {
		/* We get the entry to work with. */
		current_entry_offset = (uint32_t *)(memmap_start + memmap_location);
		/* The first 4 bytes are the length of this entry. */
		entry_size = current_entry_offset[0];
		/* Next 8 are the start of the block. */
		entry_start_l = current_entry_offset[1];
// 		entry_start_h = current_entry_offset[2];
		/* Next 8 are the entry length. */
		entry_length_l = current_entry_offset[3];
// 		entry_length_h = current_entry_offset[4];
		/* The rest are the type. */
		entry_type = current_entry_offset[5];
		
		/* We now have the entry, let's make a chunk if it's at or 
		 * past the 1M mark and below 4G and 4K aligned. */
		if (entry_start_l >= 0x100000 && entry_type == 1) {
			//__asm__("xchgw %bx, %bx");
			if ((entry_start_l <= kernel_start) & ((entry_start_l+entry_length_l) > kernel_end)) {
				/* Entry starts at or in the kernel, but goes past the end. */
				free_page_array[current_chunk].position = (kernel_end & 0xFFFFF000) | 0x00000001;
				free_page_array[current_chunk].length = (entry_length_l - (kernel_end - kernel_start)) & 0xFFFFF000;
				current_chunk++;
			} else {
				if (entry_start_l > kernel_end) {
				free_page_array[current_chunk].position = (entry_start_l & 0xFFFFF000) | 0x00000001;
				free_page_array[current_chunk].length = entry_length_l & 0xFFFFF000;
				current_chunk++;
				}
			}
		}
		
		/* Now, we move to the next entry by adding the size of this one. */
		memmap_location += entry_size + 4; /* The entra 4 is to account for the size itself. */
	}
	
	/* Now we need to specifically remove the kernel's address space. We 
	 * first find the chunk that holds the kernel. */
}
