/* 
 * vga.c
 * Daniel Selmes 2016
 * Provides basic console functionality for VGA mode 3 screen output
*/

/* Include my own header */
#include <vga.h>

/* Assembles color data for a VGA character */
uint8_t make_color_data(enum vga_color foreground, enum vga_color background)
{
	return (background << 4) | foreground;
}

/* Assembles a VGA character entry (with color data) */
uint16_t make_entry_data(char c, uint8_t colordata)
{
	return (uint16_t)(((uint16_t)colordata << 8) | (uint16_t)c);
}

/*
 * Initialises the terminal variables and clears the screen buffer.
*/
void terminal_init()
{
	terminal_row = 0;
	terminal_col = 0;
	terminal_color = make_color_data(7, 0); /* Light grey on black. */
	terminal_buffer = (uint16_t *)VGA_BUFFER_ADDRESS;
	/* This should be done better, but for now... */
	for (int index = 0; index < ROWS * COLS; index++) {
		terminal_buffer[index] = make_entry_data(' ', terminal_color);
	}
}

/* Sets the terminal printing color */
void terminal_set_color(uint8_t color)
{
	terminal_color = color;
}

/* Sets the terminal position */
void terminal_set_pos(uint8_t col, uint8_t row)
{
	terminal_col = col;
	terminal_row = row;
	terminal_update_cursor();
}

/* Increment the character position by 1 */
void terminal_increment_pos()
{
	terminal_col++;
	if (terminal_col >= COLS) {
		terminal_col = 0;
		terminal_row++;
		if (terminal_row >= ROWS) {
			terminal_scroll();
			terminal_row--;
		}
	}
}

/* Update the position of the cursor. */
void terminal_update_cursor()
{
// 	__asm__("xchgw %bx, %bx");
	uint16_t index = (uint16_t)terminal_col + ((uint16_t)terminal_row * COLS); /* Get cursor pos as an index */
	uint8_t index_low = (index & 0x00FF);
	uint8_t index_high = ((index & 0xFF00) >> 8);
	/* Write lower half to 03D4:0F VGA Register. */
	outb(PORT_VGA_3D4_INDEX, 0x0F);
	outb(PORT_VGA_3D4_RW, index_low);
	/* Write upper half to 03D4:0E VGA Registers. */
	outb(PORT_VGA_3D4_INDEX, 0X0E);
	outb(PORT_VGA_3D4_RW, index_high);
}

/* Scroll the terminal up by 1 line. */
void terminal_scroll() 
{
	memmove((void *)terminal_buffer, (void *)(terminal_buffer + COLS), sizeof(uint16_t)*COLS*ROWS);
}

/* Puts an entry on the screen */
void terminal_put_entry(uint16_t entry) 
{
	terminal_put_entry_at(entry, terminal_col, terminal_row);
}

/* Puts an entry on the screen at the given point */
void terminal_put_entry_at(uint16_t entry, uint8_t col, uint8_t row) 
{
	terminal_buffer[row * COLS + col] = entry;
	terminal_increment_pos();
}

/* Print a character on the screen. */
void terminal_putc(char c) 
{
	switch (c) {
	  case '\n':
		/* Newline. */
		terminal_col = COLS;
		terminal_increment_pos();
		break;
	  default:
		/* Normal Characters. */
		terminal_put_entry(make_entry_data(c, terminal_color));
		break;
	}
}

/* Print a character to the screen at the given position. */
void terminal_putc_at(char c, uint8_t col, uint8_t row)
{
	terminal_put_entry_at(make_entry_data(c, terminal_color), col, row);
}

/* Prints a string to the terminal. */
int terminal_puts(char *string)
{
	int current_char = 0;
	while (string[current_char] != 0) {
		terminal_putc(string[current_char]);
		current_char++;
	}
	terminal_update_cursor();
	return current_char;
}

/* Prints a 32-bit value to the terminal in hex form. */
void terminal_put_hexdw(uint32_t value)
{
	for (int i = 7; i >= 0; i--) {
		uint8_t nibble = (value >> 4*i) & 0x0f;
		switch (nibble) {
			
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
				terminal_putc('0'+nibble);
				break;
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
			case 15:
				terminal_putc('A'+(nibble-10));
				break;
		}
	}
	terminal_update_cursor();
}

/* Prints a 16-bit value to the terminal in hex form. */
void terminal_put_hexw(uint16_t value)
{
	for (int i = 3; i >= 0; i--) {
		uint8_t nibble = (value >> 4*i) & 0x0f;
		switch (nibble) {
			
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
				terminal_putc('0'+nibble);
				break;
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
			case 15:
				terminal_putc('A'+(nibble-10));
				break;
		}
	}
	terminal_update_cursor();
}

/* Prints a 8-bit value to the terminal in hex form. */
void terminal_put_hexb(uint8_t value)
{
	for (int i = 1; i >= 0; i--) {
		uint8_t nibble = (value >> 4*i) & 0x0f;
		switch (nibble) {
			
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
				terminal_putc('0'+nibble);
				break;
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
			case 15:
				terminal_putc('A'+(nibble-10));
				break;
		}
	}
	terminal_update_cursor();
}

/* Prints the hex value of an arbitrary size data type, you must provide a 
 * pointer and size in bytes of the object. Works only for up to maximum size 
 * integer supported. */
void terminal_put_hex_a(uintmax_t value, int size)
{
	for (int i = (size*2)-1; i >= 0; i--) {
		uint8_t nibble = (value >> 4*i) & 0x0f;
		switch (nibble) {
			
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
				terminal_putc('0'+nibble);
				break;
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
			case 15:
				terminal_putc('A'+(nibble-10));
				break;
		}
	}
	terminal_update_cursor();
}
