/*
 * page_allocator.c
 * Daniel Selmes 2016
 * Memory management at the page level, including page allocationa and
 * de-allocation, virtualisation of memory and more. 
*/

/* Includes */
#include <page_allocator.h>
#include <memmap.h>

struct free_page_chunk free_pages_array[1024];