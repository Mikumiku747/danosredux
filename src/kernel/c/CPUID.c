/* CPUID.c
 * Daniel Selmes 2016
 * Functions for checking and testing CPU features using the CPUID
 * instruction. 
*/

/* Includes. */
#include <CPUID.h>
#include <vga.h>

/* dump_vendor_string
 * Prints the vendor string to the console. */
void dump_vendor_string()
{
	/* First, get the actual values. */
	uint32_t eax, ebx, ecx, edx;
	read_CPUID(0, &eax, &ebx, &ecx, &edx);
	/* Now encode them into a string and print it off. */
	char vendorID[13] = { '\0' };
	for (int i = 0; i < 4; i++) {
		vendorID[i] = (char)(ebx >> (4*i));
		vendorID[i*2] = (char)(ecx >> (4*i));
		vendorID[i*3] = (char)(ecx >> (4*i));
	}
	terminal_puts(vendorID);
	return;
}

/* dump_CPU_features. 
 * Prints the features the CPU supports to the console. */
void dump_CPU_features()
{
	/* Get the vendor, so we know how to get CPU features. */
	uint32_t eax, ebx, ecx, edx;
	read_CPUID(0, &eax, &ebx, &ecx, &edx);
	/* Now, depending on the processor, print off features. */
	if (ebx == 0x756E6547) {
		/* Intel CPU. We get features using code 1 and reading ECX and 
		 * EDX. */
		read_CPUID(1, &eax, &ebx, &ecx, &edx);
		terminal_puts("[GenuineIntel] ");
		if ((edx & 0x00000001) != 0) {terminal_puts("FPU ");}
		if ((edx & 0x00000002) != 0) {terminal_puts("VME ");}
		if ((edx & 0x00000004) != 0) {terminal_puts("DE ");}
		if ((edx & 0x00000008) != 0) {terminal_puts("PSE ");}
		if ((edx & 0x00000010) != 0) {terminal_puts("TSC ");}
		if ((edx & 0x00000020) != 0) {terminal_puts("MSR ");}
		if ((edx & 0x00000040) != 0) {terminal_puts("PAE ");}
		if ((edx & 0x00000080) != 0) {terminal_puts("MCE ");}
		if ((edx & 0x00000100) != 0) {terminal_puts("CX8 ");}
		if ((edx & 0x00000200) != 0) {terminal_puts("APIC ");}
		if ((edx & 0x00000800) != 0) {terminal_puts("SEP ");}
		if ((edx & 0x00001000) != 0) {terminal_puts("MTRR ");}
		if ((edx & 0x00002000) != 0) {terminal_puts("PGE ");}
		if ((edx & 0x00004000) != 0) {terminal_puts("MCA ");}
		if ((edx & 0x00008000) != 0) {terminal_puts("CMOV ");}
		if ((edx & 0x00010000) != 0) {terminal_puts("PAT ");}
		if ((edx & 0x00020000) != 0) {terminal_puts("PSE-36 ");}
		if ((edx & 0x00040000) != 0) {terminal_puts("PSN ");}
		if ((edx & 0x00080000) != 0) {terminal_puts("CFLSH ");}
		if ((edx & 0x00200000) != 0) {terminal_puts("DS ");}
		if ((edx & 0x00400000) != 0) {terminal_puts("ACPI ");}
		if ((edx & 0x00800000) != 0) {terminal_puts("MMX ");}
		if ((edx & 0x01000000) != 0) {terminal_puts("FXSR ");}
		if ((edx & 0x02000000) != 0) {terminal_puts("SSE ");}
		if ((edx & 0x04000000) != 0) {terminal_puts("SSE2 ");}
		if ((edx & 0x08000000) != 0) {terminal_puts("SS ");}
		if ((edx & 0x10000000) != 0) {terminal_puts("HTT ");}
		if ((edx & 0x20000000) != 0) {terminal_puts("TM ");}
		return;
	}
	if (ebx == 0x68747541) {
		/* AMD CPU, pretty much the same method as intel, but 
		 * there's a few defined in ECX as well. */
		read_CPUID(1, &eax, &ebx, &ecx, &edx);
		terminal_puts("[AuthenticAMD]");
		if ((edx & 0x00000001) != 0) {terminal_puts("FPU ");}
		if ((edx & 0x00000002) != 0) {terminal_puts("VME ");}
		if ((edx & 0x00000004) != 0) {terminal_puts("DE ");}
		if ((edx & 0x00000008) != 0) {terminal_puts("PSE ");}
		if ((edx & 0x00000010) != 0) {terminal_puts("TSC ");}
		if ((edx & 0x00000020) != 0) {terminal_puts("MSR ");}
		if ((edx & 0x00000040) != 0) {terminal_puts("PAE ");}
		if ((edx & 0x00000080) != 0) {terminal_puts("MCE ");}
		if ((edx & 0x00000100) != 0) {terminal_puts("CX8 ");}
		if ((edx & 0x00000200) != 0) {terminal_puts("APIC ");}
		if ((edx & 0x00000800) != 0) {terminal_puts("SEP ");}
		if ((edx & 0x00001000) != 0) {terminal_puts("MTRR ");}
		if ((edx & 0x00002000) != 0) {terminal_puts("PGE ");}
		if ((edx & 0x00004000) != 0) {terminal_puts("MCA ");}
		if ((edx & 0x00008000) != 0) {terminal_puts("CMOV ");}
		if ((edx & 0x00010000) != 0) {terminal_puts("PAT ");}
		if ((edx & 0x00020000) != 0) {terminal_puts("PSE-36 ");}
		if ((edx & 0x00040000) != 0) {terminal_puts("PSN ");}
		if ((edx & 0x00080000) != 0) {terminal_puts("CFLSH ");}
		if ((edx & 0x00200000) != 0) {terminal_puts("DS ");}
		if ((edx & 0x00400000) != 0) {terminal_puts("ACPI ");}
		if ((edx & 0x00800000) != 0) {terminal_puts("MMX ");}
		if ((edx & 0x01000000) != 0) {terminal_puts("FXSR ");}
		if ((edx & 0x02000000) != 0) {terminal_puts("SSE ");}
		if ((edx & 0x04000000) != 0) {terminal_puts("SSE2 ");}
		if ((edx & 0x08000000) != 0) {terminal_puts("SS ");}
		if ((edx & 0x10000000) != 0) {terminal_puts("HTT ");}
		if ((edx & 0x20000000) != 0) {terminal_puts("TM ");}
		
		if ((ecx & 0x00000001) != 0) {terminal_puts("SSE3 ");}
		if ((ecx & 0x00000002) != 0) {terminal_puts("PCLMULQDQ ");}
		if ((ecx & 0x00000008) != 0) {terminal_puts("MONITOR ");}
		if ((ecx & 0x00000200) != 0) {terminal_puts("SSSE3 ");}
		if ((ecx & 0x00001000) != 0) {terminal_puts("FMA ");}
		if ((ecx & 0x00002000) != 0) {terminal_puts("CMPXCHG16B ");}
		if ((ecx & 0x00080000) != 0) {terminal_puts("SSE41 ");}
		if ((ecx & 0x00100000) != 0) {terminal_puts("SSE42 ");}
		if ((ecx & 0x00800000) != 0) {terminal_puts("POPCNT ");}
		if ((ecx & 0x02000000) != 0) {terminal_puts("AES ");}
		if ((ecx & 0x04000000) != 0) {terminal_puts("XSAVE ");}
		if ((ecx & 0x08000000) != 0) {terminal_puts("OSXSAVE ");}
		if ((ecx & 0x10000000) != 0) {terminal_puts("AVX ");}
		if ((ecx & 0x20000000) != 0) {terminal_puts("F16C ");}
		return;
	}
	/* If we get here, it means I don't have the code for detecting 
	 * features yet, so we'll just return an error. */
	terminal_puts("[Unknown     ]");
	return;
}