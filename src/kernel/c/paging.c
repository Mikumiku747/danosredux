/* paging.c
 * Daniel Selmes 2016
 * Contains functions and arrays related to the core mechanics of paging in
 * the system. Will be closely related to the page allocation code and data. 
*/

/* Includes. */
#include <stdint.h>
#include <paging.h>
#include <vga.h>
#include <paging_asm.h>
#include <idt.h>

/* Definitions of some page directories/tables which will certainly exist. */
/* The main page directory for when the main kernel code is running. */
struct page_directory_entry kernel_master_page_directory[1024] __attribute__ ((aligned (4096)));
/* The main page table which protects kernel code and memory mapped IO. */
struct page_table_entry kernel_reserved_page_table[1024] __attribute__ ((aligned (4096)));

/* initialise_kernel_paging 
 * Initialises basic paging functionality by filling out some pre-allocated
 * page directories and tables, identity mapping the first 1MB and the main
 * kernel code, and enabling paging on the processor. You should install a 
 * very basic page fault interrupt handler before calling this, or you'll 
 * likely be getting triple faults. kernel_start and kernel_end are addresses 
 * for the start and end of the kernel. They should be page aligned, that is, 
 * aligned to the nearest 4k unit which covers the whole kernel. If the linker
 * is nice it should define them aligned, but in case it doesn't, this will 
 * check too. They also both need to be less than 4MB, or 0x00400000. Note
 * kernel_end should be the first 4K aligned address which the kernel is not
 * a part of. */
void initialise_kernel_paging(uint32_t kernel_start, uint32_t kernel_end) 
{
	/* Begin by filling out the default page directories and page tables 
	 * with default values. */
	for (int i = 0; i < 1024; i++) {
		((uint32_t *)kernel_master_page_directory)[i] = (PAGING_DIRECTORY_WRITEABLE);
		((uint32_t *)kernel_reserved_page_table)[i] = (PAGING_PAGE_WRITEABLE);
	}
	/* Now we'll fill out the kernel reserved page table with identity
	 * mapping for the first 1MB. */
	for (int page = 0; page < 256; page++) {
		((uint32_t *)kernel_reserved_page_table)[page] = (PAGING_PAGE_PRESENT |
						    PAGING_PAGE_WRITEABLE |
						    (page * 0x1000));
	};
	/* Now we add the kernel reserved areas in. */
	volatile uint32_t first_page = (kernel_start & 0xFFFFF000);
	volatile uint32_t last_page;
	if ((kernel_end & 0x00000FFF) != 0) {
		last_page = ((kernel_end & 0xFFFFF000) + 0x00001000);
	} else {
		last_page = (kernel_end & 0xFFFFF000);
	}
	for (uint32_t page = first_page; page < last_page; page += 0x1000) {
		((uint32_t *)kernel_reserved_page_table)[(page / 0x1000)] = ( PAGING_PAGE_PRESENT |
								PAGING_PAGE_WRITEABLE |
								page);
	}
	/* Now we need to add our page table to the page directory. The first 
	 * table maps the first 4MB, which is what our page table covers. */
	kernel_master_page_directory[0].present = 0x01;
	kernel_master_page_directory[0].writeable = 0x01;
	kernel_master_page_directory[0].page_table_addr = (uint32_t)(&kernel_reserved_page_table) >> 12;
	/* Set the last page to reference the paging directory itself, this 
	 * hack allows us to edit the paging table at any time by editing
	 * memory in virtual addresses 0xFFFFF000 to 0xFFFFFFFF. */
	kernel_master_page_directory[1023].present = 0x01;
	kernel_master_page_directory[1023].writeable = 0x01;
	kernel_master_page_directory[1023].page_table_addr = (uint32_t)(&kernel_master_page_directory) >> 12;
	/* Now, we need to prepare for the worst, and install a page fault 
	 * exception handler, so we don't get any terrible consequences. */
	idt_encode_entry(INTERRUPT_EXCEPTION + INTERRUPT_PAGE_FAULT, isr_handler_page_fault, 0x08, IDT_FLAGS_INT32);
	/* Now, we can actually finally enable paging. */
	install_paging_directory((uint32_t)(&kernel_master_page_directory));
	set_paging_bit();
}

/* page_fault_handler
 * Function which handles page fault exceptions, is passed some values by the 
 * CPU and an assembly wrapper. */
void page_fault_handler(uint32_t fault_address)
{
	terminal_puts("\nPAGE FAULT at 0x");
	terminal_put_hexdw(fault_address);
	terminal_puts(", VERY BAD, DO NOT WANT!!!\n");
}
