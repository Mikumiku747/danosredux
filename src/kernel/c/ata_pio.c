/*
 * ata_pio.c
 * Daniel Selmes 2016
 * ATA Port I/O functions, for reading and writing to the disk early in 
 * system lifetime. */

/* Includes */
#include <ata_pio.h>
#include <io.h>
#include <pci.h>
#include <vga.h>

/* Some data structures for keeping track of drive state. */
/* Keeps track of the drive type (and presence) for the IDE controller. 
 * 0:Not Present  1:ATA  2:ATAPI  3:SATA  4:SATAPI */
uint8_t IDE_drive_type[4];

/* Used to read IDE drive state into from an IDENTIFY command. */
uint16_t identify_results[4][256];

/* ATA_PIO_initialise
 * Initialises ATA PIO related data, including locating and detecting drives
 * and preparing them for use. Returns 0 on success, Anything else is an 
 * error. */
uint32_t ATA_PIO_initialise()
{
	/* First, we need to find out the ports of the drives. These are kept
	 * in the PCI configuration space for the IDE controller, so we need
	 * to find them on the PCI bus. We enumerate the global list to make 
	 * sure it's up to date. */
	uint8_t IDE_bus = 0xFF;
	uint8_t IDE_device = 0xFF;
	uint8_t IDE_function = 0xFF;
	uint8_t IDE_primary_port_mode;
	uint8_t IDE_secondary_port_mode;
	
	PCI_global_enumerate();
	/* Find the IDE controller on the PCI bus. */
	for (uint8_t i = 0; i < PCI_global_device_count; i++) {
		if ((PCI_global_device_list[i].class == 0x01) && (PCI_global_device_list[i].subclass == 0x01)) {
			/* Found it, let's keep a tabs on which bus/device/function it was. */
			IDE_bus = PCI_global_device_list[i].bus;
			IDE_device = PCI_global_device_list[i].device;
			IDE_function = PCI_global_device_list[i].function;
		}
	}
	/* If all three are still 0xFF, then we couldn't find the IDE controller. */
	if ((IDE_bus == 0xFF) && (IDE_device == 0xFF) && (IDE_function == 0xFF)) {
		return 0x00000001; /* Return "Controller not found" Error. */
	}
	/* Now we need to check what mode the controller is in, and change it
	 * to compatability mode if necessary. */
	uint32_t device_info = PCI_conf_read_data(IDE_bus, IDE_device, IDE_function, 0x08);
	/* Let's do the Primary Channel First. */
	if ((device_info & 0x00000200) == 0) {
		/* Primary mode bit is Read Only. */
		IDE_primary_port_mode = ((device_info & 0x00000100) >> 8);
	} else {
		/* Primary mode bit is R/W, set to compatability mode. */
		PCI_conf_write_data(IDE_bus, IDE_device, IDE_function, 0x08, (device_info & !0x00000100));
		IDE_primary_port_mode = 0x00;
	}
	/* Now for the secondary channel. */
	if ((device_info & 0x00000800) == 0) {
		/* Secondary mode bit is Read Only. */
		IDE_secondary_port_mode = ((device_info & 0x00000400) >> 10);
	} else {
		/* Secondary mode bit is R/W, set to compatability mode. */
		PCI_conf_write_data(IDE_bus, IDE_device, IDE_function, 0x08, (device_info & !0x00000400));
		IDE_secondary_port_mode = 0x00;
	}
	/* Now, we need to save the important IO ports so that we can use them later. */
	if (IDE_primary_port_mode == 0) {
		/* Compatability mode, ports are at known locations. */
		IDE_BASE_ADDR_PRIMARY = 0x1F0;
		IDE_CONTROL_PRIMARY = 0x3F6;
	} else {
		/* Native PCI mode, not supported... */
		IDE_BASE_ADDR_PRIMARY = 0x0;
		IDE_CONTROL_PRIMARY = 0x0;
		return 0x00000002; /* Return "Primary mode not supported" Error. */
	}
	if (IDE_secondary_port_mode == 0) {
		/* Compatability mode, ports are at known locations. */
		IDE_BASE_ADDR_SECONDARY = 0x170;
		IDE_CONTROL_SECONDARY = 0x376;
	} else {
		/* Native PCI mode, not supported... */
		IDE_BASE_ADDR_SECONDARY = 0x0;
		IDE_CONTROL_SECONDARY = 0x0;
		return 0x00000003; /* Return "Secondary mode not supported" Error. */
	}
	/* Now that we know the IO port's, We'll send an IDENTIFY to each of 
	 * the drives and try and get back info from them, such as what kind 
	 * of drive they are.  */
	uint8_t status;
	/* First Primary Master Drive. */
	outb(IDE_PRIMARY_DRIVESEL, 0xA0); /* Select Master Drive. */
	inb(IDE_PRIMARY_ALT_STATUS);
	inb(IDE_PRIMARY_ALT_STATUS);
	inb(IDE_PRIMARY_ALT_STATUS);
	inb(IDE_PRIMARY_ALT_STATUS); /* Read in status 4 times to give time for the drives to switch. */
	outb(IDE_PRIMARY_SECTOR_COUNT, 0x00);
	outb(IDE_PRIMARY_LBALOW, 0x00);
	outb(IDE_PRIMARY_LBAMID, 0x00);
	outb(IDE_PRIMARY_LBAHIGH, 0x00); /* Set SectorCount and LBA ports to zero. */
	outb(IDE_PRIMARY_COMMAND, 0xEC); /* Send the IDENTIFY command. */
	status = inb(IDE_PRIMARY_STATUS);
	if (status == 0) {
		IDE_drive_type[0] = 0; /* Not Present. */
	} else if ((status & 0x01) != 0x00) {
		/* An error was immediately sent, let's check to see if it's ATAPI. */
		if ((inb(IDE_PRIMARY_LBAMID) == 0x14) && (inb(IDE_PRIMARY_LBAHIGH) == 0xEB)) {
			/* It's ATAPI, we need to send the IDENTIFY PACKET command instead. */
			outb(IDE_PRIMARY_DRIVESEL, 0xA0); /* Select Master Drive. */
			inb(IDE_PRIMARY_ALT_STATUS);
			inb(IDE_PRIMARY_ALT_STATUS);
			inb(IDE_PRIMARY_ALT_STATUS);
			inb(IDE_PRIMARY_ALT_STATUS); /* Read in status 4 times to give time for the drives to switch. */
			outb(IDE_PRIMARY_SECTOR_COUNT, 0x00);
			outb(IDE_PRIMARY_LBALOW, 0x00);
			outb(IDE_PRIMARY_LBAMID, 0x00);
			outb(IDE_PRIMARY_LBAHIGH, 0x00); /* Set SectorCount and LBA ports to zero. */
			outb(IDE_PRIMARY_COMMAND, 0xA1); /* Send the IDENTIFY PACKET command. */
			status = inb(IDE_PRIMARY_STATUS);
			/* Wait for the BUSY bit to clear. */
			while ((status = inb(IDE_PRIMARY_STATUS) & 0x80) != 0);
			/* Now we wait for DRQ or ERR to set. */
			while ((status = inb(IDE_PRIMARY_STATUS) & 0x09) == 0);
			/* Check to see if the ERR flag set. */
			if ((status & 0x01) != 0) {
				/* Even the IDENTIFY PACKET command failed, welp. */
				IDE_drive_type[0] = 0; /* Not Present. */
			} else {
				/* OK, let's read out all the ATAPI info. */
				for (int i = 0; i < 256; i++) {
					identify_results[0][i] = inw(IDE_PRIMARY_DATA);
				}
				IDE_drive_type[0] = 2; /* ATAPI. */
			}
		}
		if ((inb(IDE_PRIMARY_LBAMID) == 0x3C) && (inb(IDE_PRIMARY_LBAHIGH) == 0xC3)) {
			/* It's SATA, we need to take a different approach... */
			IDE_drive_type[0] = 3; /* SATA. */
		}
		if ((inb(IDE_PRIMARY_LBAMID) == 0x69) && (inb(IDE_PRIMARY_LBAHIGH) == 0x96)) {
			/* It's SATAPI, we need to take a different approach... */
			IDE_drive_type[0] = 4; /* SATAPI. */
		}
	} else {
		/* Wait for the BUSY bit to clear. */
		while ((status = inb(IDE_PRIMARY_STATUS) & 0x80) != 0);
		if ((inb(IDE_PRIMARY_LBAHIGH) != 0x00) || (inb(IDE_PRIMARY_LBAMID) != 0x00)) {
			/* Drive is not ATA. Probably a badly programmed ATAPI Drive. */
			IDE_drive_type[0] = 0; /* Not present. Give up for now. */
		} else {
			/* ATA Drive, let's wait for it to finish the command. */
			while ((inb(IDE_PRIMARY_STATUS) & 0x09) == 0);
			/* Now check to see if we are done or got an error. */
			status = inb(IDE_PRIMARY_STATUS);
			if ((status & 0x01) != 0x00) {
				/* There was a drive error. Just give up for now. */
				IDE_drive_type[0] = 0; /* Not Present. */
			} else {
				/* The drive is ready to have the identification stuff read out of it. */
				for (int i = 0; i < 256; i++) {
					identify_results[0][i] = inw(IDE_PRIMARY_DATA);
				}
				IDE_drive_type[0] = 1; /* ATA. */
			}
		}
	}
	/* Now for the Primary Slave Drive. */
	outb(IDE_PRIMARY_DRIVESEL, 0xB0); /* Select Slave Drive. */
	inb(IDE_PRIMARY_ALT_STATUS);
	inb(IDE_PRIMARY_ALT_STATUS);
	inb(IDE_PRIMARY_ALT_STATUS);
	inb(IDE_PRIMARY_ALT_STATUS); /* Read in status 4 times to give time for the drives to switch. */
	outb(IDE_PRIMARY_SECTOR_COUNT, 0x00);
	outb(IDE_PRIMARY_LBALOW, 0x00);
	outb(IDE_PRIMARY_LBAMID, 0x00);
	outb(IDE_PRIMARY_LBAHIGH, 0x00); /* Set SectorCount and LBA ports to zero. */
	outb(IDE_PRIMARY_COMMAND, 0xEC); /* Send the IDENTIFY command. */
	status = inb(IDE_PRIMARY_STATUS);
	if (status == 0) {
		IDE_drive_type[1] = 0; /* Not Present. */
	} else if ((status & 0x01) != 0x00) {
		/* An error was immediately sent, let's check to see if it's ATAPI. */
		if ((inb(IDE_PRIMARY_LBAMID) == 0x14) && (inb(IDE_PRIMARY_LBAHIGH) == 0xEB)) {
			/* It's ATAPI, we need to send the IDENTIFY PACKET command instead. */
			outb(IDE_PRIMARY_DRIVESEL, 0xB0); /* Select Slave Drive. */
			inb(IDE_PRIMARY_ALT_STATUS);
			inb(IDE_PRIMARY_ALT_STATUS);
			inb(IDE_PRIMARY_ALT_STATUS);
			inb(IDE_PRIMARY_ALT_STATUS); /* Read in status 4 times to give time for the drives to switch. */
			outb(IDE_PRIMARY_SECTOR_COUNT, 0x00);
			outb(IDE_PRIMARY_LBALOW, 0x00);
			outb(IDE_PRIMARY_LBAMID, 0x00);
			outb(IDE_PRIMARY_LBAHIGH, 0x00); /* Set SectorCount and LBA ports to zero. */
			outb(IDE_PRIMARY_COMMAND, 0xA1); /* Send the IDENTIFY PACKET command. */
			status = inb(IDE_PRIMARY_STATUS);
			/* Wait for the BUSY bit to clear. */
			while ((status = inb(IDE_PRIMARY_STATUS) & 0x80) != 0);
			/* Now we wait for DRQ or ERR to set. */
			while ((status = inb(IDE_PRIMARY_STATUS) & 0x09) == 0);
			/* Check to see if the ERR flag set. */
			if ((status & 0x01) != 0) {
				/* Even the IDENTIFY PACKET command failed, welp. */
				IDE_drive_type[1] = 0; /* Not Present. */
			} else {
				/* OK, let's read out all the ATAPI info. */
				for (int i = 0; i < 256; i++) {
					identify_results[1][i] = inw(IDE_PRIMARY_DATA);
				}
				IDE_drive_type[1] = 2; /* ATAPI. */
			}
		}
		if ((inb(IDE_PRIMARY_LBAMID) == 0x3C) && (inb(IDE_PRIMARY_LBAHIGH) == 0xC3)) {
			/* It's SATA, we need to take a different approach... */
			IDE_drive_type[1] = 3; /* SATA. */
		}
		if ((inb(IDE_PRIMARY_LBAMID) == 0x69) && (inb(IDE_PRIMARY_LBAHIGH) == 0x96)) {
			/* It's SATAPI, we need to take a different approach... */
			IDE_drive_type[1] = 4; /* SATAPI. */
		}
	} else {
		/* Wait for the BUSY bit to clear. */
		while ((status = inb(IDE_PRIMARY_STATUS) & 0x80) != 0);
		if ((inb(IDE_PRIMARY_LBAHIGH) != 0x00) || (inb(IDE_PRIMARY_LBAMID) != 0x00)) {
			/* Drive is not ATA. Probably a badly programmed ATAPI Drive. */
			IDE_drive_type[1] = 0; /* Not present. Give up for now. */
		} else {
			/* ATA Drive, let's wait for it to finish the command. */
			while ((inb(IDE_PRIMARY_STATUS) & 0x09) == 0);
			/* Now check to see if we are done or got an error. */
			status = inb(IDE_PRIMARY_STATUS);
			if ((status & 0x01) != 0x00) {
				/* There was a drive error. Just give up for now. */
				IDE_drive_type[1] = 0; /* Not Present. */
			} else {
				/* The drive is ready to have the identification stuff read out of it. */
				for (int i = 0; i < 256; i++) {
					identify_results[1][i] = inw(IDE_PRIMARY_DATA);
				}
				IDE_drive_type[1] = 1; /* ATA. */
			}
		}
	}
	/* Do the Secondary Master Drive. */
	outb(IDE_SECONDARY_DRIVESEL, 0xA0); /* Select Master Drive. */
	inb(IDE_SECONDARY_ALT_STATUS);
	inb(IDE_SECONDARY_ALT_STATUS);
	inb(IDE_SECONDARY_ALT_STATUS);
	inb(IDE_SECONDARY_ALT_STATUS); /* Read in status 4 times to give time for the drives to switch. */
	outb(IDE_SECONDARY_SECTOR_COUNT, 0x00);
	outb(IDE_SECONDARY_LBALOW, 0x00);
	outb(IDE_SECONDARY_LBAMID, 0x00);
	outb(IDE_SECONDARY_LBAHIGH, 0x00); /* Set SectorCount and LBA ports to zero. */
	outb(IDE_SECONDARY_COMMAND, 0xEC); /* Send the IDENTIFY command. */
	status = inb(IDE_SECONDARY_STATUS);
	if (status == 0) {
		IDE_drive_type[2] = 0; /* Not Present. */
	} else if ((status & 0x01) != 0x00) {
		/* An error was immediately sent, let's check to see if it's ATAPI. */
		if ((inb(IDE_SECONDARY_LBAMID) == 0x14) && (inb(IDE_SECONDARY_LBAHIGH) == 0xEB)) {
			/* It's ATAPI, we need to send the IDENTIFY PACKET command instead. */
			outb(IDE_SECONDARY_DRIVESEL, 0xA0); /* Select Master Drive. */
			inb(IDE_SECONDARY_ALT_STATUS);
			inb(IDE_SECONDARY_ALT_STATUS);
			inb(IDE_SECONDARY_ALT_STATUS);
			inb(IDE_SECONDARY_ALT_STATUS); /* Read in status 4 times to give time for the drives to switch. */
			outb(IDE_SECONDARY_SECTOR_COUNT, 0x00);
			outb(IDE_SECONDARY_LBALOW, 0x00);
			outb(IDE_SECONDARY_LBAMID, 0x00);
			outb(IDE_SECONDARY_LBAHIGH, 0x00); /* Set SectorCount and LBA ports to zero. */
			outb(IDE_SECONDARY_COMMAND, 0xA1); /* Send the IDENTIFY PACKET command. */
			status = inb(IDE_SECONDARY_STATUS);
			/* Wait for the BUSY bit to clear. */
			while ((status = inb(IDE_SECONDARY_STATUS) & 0x80) != 0);
			/* Now we wait for DRQ or ERR to set. */
			while ((status = inb(IDE_SECONDARY_STATUS) & 0x09) == 0);
			/* Check to see if the ERR flag set. */
			if ((status & 0x01) != 0) {
				/* Even the IDENTIFY PACKET command failed, welp. */
				IDE_drive_type[2] = 0; /* Not Present. */
			} else {
				/* OK, let's read out all the ATAPI info. */
				for (int i = 0; i < 256; i++) {
					identify_results[2][i] = inw(IDE_SECONDARY_DATA);
				}
				IDE_drive_type[2] = 2; /* ATAPI. */
			}
		}
		if ((inb(IDE_SECONDARY_LBAMID) == 0x3C) && (inb(IDE_SECONDARY_LBAHIGH) == 0xC3)) {
			/* It's SATA, we need to take a different approach... */
			IDE_drive_type[2] = 3; /* SATA. */
		}
		if ((inb(IDE_SECONDARY_LBAMID) == 0x69) && (inb(IDE_SECONDARY_LBAHIGH) == 0x96)) {
			/* It's SATAPI, we need to take a different approach... */
			IDE_drive_type[2] = 4; /* SATAPI. */
		}
	} else {
		/* Wait for the BUSY bit to clear. */
		while ((status = inb(IDE_SECONDARY_STATUS) & 0x80) != 0);
		if ((inb(IDE_SECONDARY_LBAHIGH) != 0x00) || (inb(IDE_SECONDARY_LBAMID) != 0x00)) {
			/* Drive is not ATA. Probably a badly programmed ATAPI Drive. */
			IDE_drive_type[2] = 0; /* Not present. Give up for now. */
		} else {
			/* ATA Drive, let's wait for it to finish the command. */
			while ((inb(IDE_SECONDARY_STATUS) & 0x09) == 0);
			/* Now check to see if we are done or got an error. */
			status = inb(IDE_SECONDARY_STATUS);
			if ((status & 0x01) != 0x00) {
				/* There was a drive error. Just give up for now. */
				IDE_drive_type[2] = 0; /* Not Present. */
			} else {
				/* The drive is ready to have the identification stuff read out of it. */
				for (int i = 0; i < 256; i++) {
					identify_results[2][i] = inw(IDE_SECONDARY_DATA);
				}
				IDE_drive_type[2] = 1; /* ATA. */
			}
		}
	}
	/* Now for the secondary Slave Drive. */
	outb(IDE_SECONDARY_DRIVESEL, 0xB0); /* Select Slave Drive. */
	inb(IDE_SECONDARY_ALT_STATUS);
	inb(IDE_SECONDARY_ALT_STATUS);
	inb(IDE_SECONDARY_ALT_STATUS);
	inb(IDE_SECONDARY_ALT_STATUS); /* Read in status 4 times to give time for the drives to switch. */
	outb(IDE_SECONDARY_SECTOR_COUNT, 0x00);
	outb(IDE_SECONDARY_LBALOW, 0x00);
	outb(IDE_SECONDARY_LBAMID, 0x00);
	outb(IDE_SECONDARY_LBAHIGH, 0x00); /* Set SectorCount and LBA ports to zero. */
	outb(IDE_SECONDARY_COMMAND, 0xEC); /* Send the IDENTIFY command. */
	status = inb(IDE_SECONDARY_STATUS);
	if (status == 0) {
		IDE_drive_type[3] = 0; /* Not Present. */
	} else if ((status & 0x01) != 0x00) {
		/* An error was immediately sent, let's check to see if it's ATAPI. */
		if ((inb(IDE_SECONDARY_LBAMID) == 0x14) && (inb(IDE_SECONDARY_LBAHIGH) == 0xEB)) {
			/* It's ATAPI, we need to send the IDENTIFY PACKET command instead. */
			outb(IDE_SECONDARY_DRIVESEL, 0xB0); /* Select Master Drive. */
			inb(IDE_SECONDARY_ALT_STATUS);
			inb(IDE_SECONDARY_ALT_STATUS);
			inb(IDE_SECONDARY_ALT_STATUS);
			inb(IDE_SECONDARY_ALT_STATUS); /* Read in status 4 times to give time for the drives to switch. */
			outb(IDE_SECONDARY_SECTOR_COUNT, 0x00);
			outb(IDE_SECONDARY_LBALOW, 0x00);
			outb(IDE_SECONDARY_LBAMID, 0x00);
			outb(IDE_SECONDARY_LBAHIGH, 0x00); /* Set SectorCount and LBA ports to zero. */
			outb(IDE_SECONDARY_COMMAND, 0xA1); /* Send the IDENTIFY PACKET command. */
			status = inb(IDE_SECONDARY_STATUS);
			/* Wait for the BUSY bit to clear. */
			while ((status = inb(IDE_SECONDARY_STATUS) & 0x80) != 0);
			/* Now we wait for DRQ or ERR to set. */
			while ((status = inb(IDE_SECONDARY_STATUS) & 0x09) == 0);
			/* Check to see if the ERR flag set. */
			if ((status & 0x01) != 0) {
				/* Even the IDENTIFY PACKET command failed, welp. */
				IDE_drive_type[3] = 0; /* Not Present. */
			} else {
				/* OK, let's read out all the ATAPI info. */
				for (int i = 0; i < 256; i++) {
					identify_results[3][i] = inw(IDE_SECONDARY_DATA);
				}
				IDE_drive_type[3] = 2; /* ATAPI. */
			}
		}
		if ((inb(IDE_SECONDARY_LBAMID) == 0x3C) && (inb(IDE_SECONDARY_LBAHIGH) == 0xC3)) {
			/* It's SATA, we need to take a different approach... */
			IDE_drive_type[3] = 3; /* SATA. */
		}
		if ((inb(IDE_SECONDARY_LBAMID) == 0x69) && (inb(IDE_SECONDARY_LBAHIGH) == 0x96)) {
			/* It's SATAPI, we need to take a different approach... */
			IDE_drive_type[3] = 4; /* SATAPI. */
		}
	} else {
		/* Wait for the BUSY bit to clear. */
		while ((status = inb(IDE_SECONDARY_STATUS) & 0x80) != 0);
		if ((inb(IDE_SECONDARY_LBAHIGH) != 0x00) || (inb(IDE_SECONDARY_LBAMID) != 0x00)) {
			/* Drive is not ATA. Probably a badly programmed ATAPI Drive. */
			IDE_drive_type[3] = 0; /* Not present. Give up for now. */
		} else {
			/* ATA Drive, let's wait for it to finish the command. */
			while ((inb(IDE_SECONDARY_STATUS) & 0x09) == 0);
			/* Now check to see if we are done or got an error. */
			status = inb(IDE_SECONDARY_STATUS);
			if ((status & 0x01) != 0x00) {
				/* There was a drive error. Just give up for now. */
				IDE_drive_type[3] = 0; /* Not Present. */
			} else {
				/* The drive is ready to have the identification stuff read out of it. */
				for (int i = 0; i < 256; i++) {
					identify_results[3][i] = inw(IDE_SECONDARY_DATA);
				}
				IDE_drive_type[3] = 1; /* ATA. */
			}
		}
	}
	/* Everything went according to plan, so we can return 0 for success. */
	return 0;
	
}

/* ATA_PIO_read_48
 * Reads a set of sectors into a buffer. Can be physical or virtual. Drive is
 * the drive number to use, must be from 0 to 3 (see drive macros). LBA_48 is
 * the linear block address of the address to be read. Sectors is the number
 * of sectors to be read. Buffer is the buffer for the data to be read into,
 * please ensure that it is large enough to handle the number of sectors
 * requested. Returns 0 on success or an error code: 
 * 1 - Drive Error, nonexistent or non ATA drive. 
 * 2 - Feature error, drive can't do this command. 
 * 3 - LBA error, invalid LBA (over 48 bits or too big for drive)
 * 256 - Other error. 
*/
uint32_t ATA_PIO_read_48(uint8_t drive, uint64_t  LBA_48, uint32_t sectors, void *buffer)
{
	/* To begin with, let's make sure we CAN make a read by checking
	 * arguments and drive state. */
	if ((LBA_48 & 0xFFFF000000000000) != 0x0) {
		/* Our '48 bit' LBA isn't so 48-bit, error out. */
		return 0x03;
	}
	if (drive > 0x03) {
		/* We're not acessing a real drive. */
		return 0x01;
	}
	if (IDE_drive_type[drive] != 1) {
		/* We can only do ATA_PIO reads on an ATA drive. */
		return 0x01;
	}
	if (((identify_results[drive][86] & 0x0400) == 0) |
		((identify_results[drive][87] & 0x4000) == 0 ) |
		((identify_results[drive][87] & 0x8000) != 0)) { 
		/* This drive doesn't support LBA_48. Either feature bit is
		 * not set or the feature bits are innaccurate. */
		return 0x02;
	}
	if (LBA_48 > (
		((uint64_t)(identify_results[drive][100]) << 0) ||
		((uint64_t)(identify_results[drive][101]) << 16) ||
		((uint64_t)(identify_results[drive][102]) << 32)) ) {
		/* LBA_48 is too big for this drive. */
		return 0x03;
	}
	
	/* Ok, seems good, let's set up the transfer. */
	uint16_t scount;
	if (sectors >= 65536) {
		scount = 0; 
	} else {
		scount = (uint16_t)sectors;
	}
	uint16_t port_cmd, port_ctrl;
	uint8_t drive_select_bit;
	switch (drive) {
		case 0: /* Primary Master. */
			port_cmd = IDE_BASE_ADDR_PRIMARY;
			port_ctrl = IDE_CONTROL_PRIMARY;
			drive_select_bit = 0xE0;
			break;
		case 1: /* Primary Slave. */
			port_cmd = IDE_BASE_ADDR_PRIMARY;
			port_ctrl = IDE_CONTROL_PRIMARY;
			drive_select_bit = 0xF0;
			break;
		case 2: /* Secondary Master. */
			port_cmd = IDE_BASE_ADDR_SECONDARY;
			port_ctrl = IDE_CONTROL_SECONDARY;
			drive_select_bit = 0xE0;
			break;
		case 3: /* Secondary Slave. */
			port_cmd = IDE_BASE_ADDR_SECONDARY;
			port_ctrl = IDE_CONTROL_SECONDARY;
			drive_select_bit = 0xF0;
			break;
	}
	/* Send drive select. */
	outb(port_cmd + 0x06, drive_select_bit);
	inb(port_ctrl);
	inb(port_ctrl);
	inb(port_ctrl);
	inb(port_ctrl);
	/* Send NULL to feature register. */
	outb(port_cmd + 0x01, 0x00);
	/* Send high bytes of sectorcount and LBA. */
	outb(port_cmd + 0x02, (uint8_t)(scount >> 8));
	outb(port_cmd + 0x03, (uint8_t)(LBA_48 >> 24));
	outb(port_cmd + 0x04, (uint8_t)(LBA_48 >> 32));
	outb(port_cmd + 0x05, (uint8_t)(LBA_48 >> 40));
	/* Send low bytes of sectorcount and LBA. */
	outb(port_cmd + 0x02, (uint8_t)(scount));
	outb(port_cmd + 0x03, (uint8_t)(LBA_48));
	outb(port_cmd + 0x04, (uint8_t)(LBA_48 >> 8));
	outb(port_cmd + 0x05, (uint8_t)(LBA_48 >> 16));
	/* Send the command to READ SECTORS EXT. */
	outb(port_cmd + 0x07, 0x24);
	
	/* Now we've sent the command, we need to wait for the drive to be ready to receive. */
	for (uint32_t block = 0; block < sectors; block++) {
		/* Do 4 status reads to give the drive time to set the BSY bit. */
		inb(port_ctrl);
		inb(port_ctrl);
		inb(port_ctrl);
		inb(port_ctrl);
		/* Wait for BSY to clear. */
		while ((inb(port_cmd + 0x07) & 0x80) != 0);
		/* Wait for any of DRQ, ERR or DF to set. */
		while ((inb(port_cmd + 0x07) & 0x29) == 0);
		/* Check to make sure DRQ was set. */
		if ((inb(port_cmd + 0x07) & 0x08) != 0) {
			/* DRQ is set, we can do a PIO transfer. */
			for (uint16_t word = 0; word < 256; word++) {
				((uint16_t *)buffer)[block*256 + word] = inw(port_cmd + 0x00);
				io_wait();
			}
		} else {
			/* DRQ wasn't the setting bit, read failed. */
			return 256;
		}
	}
	
	/* All sucess, return 0. */
	return 0;
}

/* ATA_PIO_read_28
 * Reads a set of sectors into a buffer, provided by a virtual address. drive
 * is the drive to use (see the drive macros), LBA_28 is a 28 bit LBA to start
 * reading from, sectors is the number of sectors to read (0 for 256 sectors), 
 * and buffer is the buffer to write the data into. Returns 0 on success or an
 * error code: 
 * 1 - Drive Error, nonexistent or non ATA drive. 
 * 2 - Feature error, drive can't do this command. 
 * 3 - LBA error, invalid LBA (over 28 bits or too big for drive)
 * 256 - Other error. 
*/
uint32_t ATA_PIO_read_28(uint8_t drive, uint32_t LBA_28, uint16_t sectors, void *buffer)
{
	/* First, validate all the arguments. */
	if (drive > 3) {
		/* Bad drive number. */
		return 0x01;
	}
	if (IDE_drive_type[drive] != 1) {
		/* It's not an ATA drive. */
		return 0x01;
	}
	if ((LBA_28 & 0xF0000000) != 0) {
		/* LBA is over 28 bits. */
		return 0x03;
	}
	if (LBA_28 > ((uint32_t)(identify_results[drive][60]) | ((uint32_t)(identify_results[drive][61]) << 16))) {
		/* LBA goes past the end of the disk. */
		return 0x03;
	}
	/* Ok, seems good, let's set up the transfer. */
	uint8_t scount;
	if (sectors >= 256) {
		scount = 0; 
	} else {
		scount = (uint8_t)sectors;
	}
	uint16_t port_cmd, port_ctrl;
	uint8_t drive_select_bit;
	switch (drive) {
		case 0: /* Primary Master. */
			port_cmd = IDE_BASE_ADDR_PRIMARY;
			port_ctrl = IDE_CONTROL_PRIMARY;
			drive_select_bit = 0xE0;
			break;
		case 1: /* Primary Slave. */
			port_cmd = IDE_BASE_ADDR_PRIMARY;
			port_ctrl = IDE_CONTROL_PRIMARY;
			drive_select_bit = 0xF0;
			break;
		case 2: /* Secondary Master. */
			port_cmd = IDE_BASE_ADDR_SECONDARY;
			port_ctrl = IDE_CONTROL_SECONDARY;
			drive_select_bit = 0xE0;
			break;
		case 3: /* Secondary Slave. */
			port_cmd = IDE_BASE_ADDR_SECONDARY;
			port_ctrl = IDE_CONTROL_SECONDARY;
			drive_select_bit = 0xF0;
			break;
	}
	/* Send drive select / LBA, and blanks. */
	outb(port_cmd + 6, drive_select_bit | ((LBA_28 >> 24) & 0x0F));
	inb(port_ctrl);
	inb(port_ctrl);
	inb(port_ctrl);
	inb(port_ctrl);
	/* Send NULL to feature register. */
	outb(port_cmd + 0x01, 0x00);
	/* Send sectorcount. */
	outb(port_cmd + 0x02, scount);
	/* Send LBA. */
	outb(port_cmd + 0x03, (uint8_t)LBA_28);
	outb(port_cmd + 0x04, (uint8_t)(LBA_28 >> 8));
	outb(port_cmd + 0x05, (uint8_t)(LBA_28 >> 16));
	/* Send the command to READ SECTORS. */
	outb(port_cmd + 0x07, 0x20);
	
	/* OK, we sent the command, now we wait for the drive to complete its request. */
	for (uint16_t block = 0; block < sectors; block++) {
		/* Do 4 status reads to give the drive time to set the BSY bit. */
		inb(port_ctrl);
		inb(port_ctrl);
		inb(port_ctrl);
		inb(port_ctrl);
		/* Wait for BSY to clear. */
		while ((inb(port_cmd + 0x07) & 0x80) != 0); 
		/* Now wait for any of DRQ, ERR or DF set. */
		while ((inb(port_cmd + 0x07) & 0x29) == 0); 
		/* OK, now if we didn't get an error we can read. */
		if ((inb(port_cmd + 0x07) & 0x08) != 0) {
			for (uint16_t word = 0; word < 256; word++) {
				/* Read in a word. */
				((uint16_t *)buffer)[block*256 + word] = inw(port_cmd + 0x00);
				io_wait();
			}
		} else {
			/* Yay, drive error... */
			return 256;
		}
	}
	
	/* Everything went well, return 0. */
	return 0;
}
