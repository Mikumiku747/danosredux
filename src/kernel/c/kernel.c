/* 
 * kernel.c
 * Daniel Selmes 2016
 * The main kernel function.
*/

#ifndef VERSION
#define VERSION "[missing version tag]"
#endif

#ifndef COMPILETIME
#define COMPILETIME "[Unknown Compile Time]"
#endif

#define VERBOSEBOOT 0

#define MBOOT_PRESENT_ONLY

/* Includes */
//#include <debug.h>
#include <vga.h>
#include <gdt.h>
#include <idt.h>
#include <io.h>
#include <pic.h>
#include <keyboard.h>
#include <input.h>
#include <multiboot.h>
#include <memmap.h>
#include <acpi.h>
#include <pci.h>
#include <ata_pio.h>
#include <paging.h>
#include <paging_asm.h>
#include <CPUID.h>

/* Special Constants */
#define ACPI_SEARCH_START 0x000E0000
#define ACPI_SEARCH_END 0x00100000

/* Declare some functions contained in this file which are directly related to
 * the kernel, such as system halt and error procedures. */
void kernel_crash(char *msg);
void kernel_halt();

/* Declare the hang function that we use from the bootstrap code to halt the 
 * system. */
extern void hang();

/* Bring in the symbols which show the start and end of the kernel. */
extern void *startOfKernel, *endOfKernel;

/* 
 * The main kernel function.
 * Takes a magic bootloader value and mboot info as arguments from the 
 * bootstrap.
*/
void kernel_main(uint32_t magic, uint32_t mbinfo_address)
{
	/* Declare pointer to the multiboot header. */
	multiboot_info_t *mbinfo;
	uint32_t startOfKernel_addr, endOfKernel_addr;
	startOfKernel_addr = (uint32_t)&startOfKernel;
	endOfKernel_addr = (uint32_t)&endOfKernel;
	
	terminal_init();
	/* State the kernel comp time. */
	terminal_puts("danosredux\n");
	terminal_puts("Compiled: " COMPILETIME "\n");
	/* Load the global descriptor table. */
	terminal_puts("Creating and loading GDT...");
	loadGDT();
	terminal_puts("Done\n");
	/* Create the IDT. */
	terminal_puts("Creating blank IDT...");
	idt_fill();
	idt_load(interrupt_descriptor_table);
	terminal_puts("Done\n");
	/* Configure the PICs. */
	terminal_puts("Configuring Programmable Interrupt Controllers...");
	PIC_remap();
	terminal_puts("Done\n");
	/* Prepare keyboard input. */
	terminal_puts("Preparing keyboard input...");
	idt_encode_entry(INTERRUPT_HARDWARE + INTERRUPT_KEYBOARD, isr_handler_keyboard, 0x08, IDT_FLAGS_INT32);
	PIC_unmask_line(1);
	__asm__("sti");
	terminal_puts("Done.\n");
	/* Get ready for a case of double fault. */
	terminal_puts("Preparing double fault handler...");
	idt_encode_entry(INTERRUPT_EXCEPTION + INTERRUPT_DOUBLE_FAULT, isr_handler_double_fault, 0x08, IDT_FLAGS_INT32);
	terminal_puts("Done.\n");
	/* Perform checks to make sure that we are running in a compatible environment. */
	terminal_puts("Checking boot environment for required features...");
	if (magic != MULTIBOOT_BOOTLOADER_MAGIC) {
#ifdef MBOOT_PRESENT_ONLY
		kernel_crash("The kernel was loaded from a non-multiboot bootloader. You must use a multiboot compliant loader to run this kernel.");
#else
		terminal_puts("WARNING: The kernel was loaded from a non-multiboot bootloader. You may experience crashes or failures due to missing boot environment features.\n");
#endif
	}
	mbinfo = (multiboot_info_t *)mbinfo_address;/* Get multiboot header. */
	if (!(mbinfo->flags & MULTIBOOT_INFO_MEM_MAP)) {
#ifdef MBOOT_PRESENT_ONLY
		kernel_crash("Multiboot memory map information is missing. Please ensure your bootloader provides memory map information according to the multiboot standard.");
#else
		terminal_puts("WARNING: There was no multiboot memory map detected. You may experience crashes or failures due to missing boot environment features.\n");
#endif
	}
	terminal_puts("Done.\n");
	
	/* Provide details on bootloader environment. */
#if VERBOSEBOOT == 1
	terminal_puts("Bootloader info: \n    Magic: 0x");
	terminal_put_hexdw(magic);
	terminal_puts("\n    Info table: 0x");
	terminal_put_hexdw(mbinfo_address);
	terminal_puts(" \n");
#endif
	
	/* Debug print of the bootloader's memory map. */
#if VERBOSEBOOT == 1
	print_bios_memmap(mbinfo->mmap_length, mbinfo->mmap_addr);
#endif
	
	/* Print out the space taken by the kernel. Note the pointer address
	 * operator on the symbols, this is because when we get these from the
	 * linker, the compiler gets the value AT these symbols, instead of 
	 * the symbols themselves (AKA it treats them as global variables). */
#if VERBOSEBOOT == 1
	terminal_puts("The kernel runs from 0x");
	terminal_put_hexdw(startOfKernel_addr);
	terminal_puts(" to 0x");
	terminal_put_hexdw(endOfKernel_addr);
	terminal_puts(". \n");
#endif
	
	/* Use the CPUID instruction to check for CPU features. */
	terminal_puts("Checking CPU supports required features...");
	if (test_for_CPUID() == 0) {
		terminal_puts("Fail, CPUID instruction not supported.\n");
	} else {
		/* Check CPU Vendor. */
		uint32_t id_eax, id_ebx, id_ecx, id_edx;
		read_CPUID(0, &id_eax, &id_ebx, &id_ecx, &id_edx);
		switch (id_ebx) {
			case 0x756E6547:
				/* Intel CPU. */
				terminal_puts("GenuineIntel. ");
				terminal_puts("Done.\n");
#if 1
				terminal_puts("CPU Supported Features:\n");
				dump_CPU_features();
				terminal_puts("\n");
#endif
				break;
			case 0x68747541:
				/* AMD CPU. */
				terminal_puts("AuthenticAMD. ");
				terminal_puts("Done.\n");
				break;
			default:
				/* Unknown CPU Vendor. */
				terminal_puts("Fail, Unknown CPU Vendor.\n");
				break;
		}
	}
	
	/* Create the free memory array. */
	initialise_free_chunks_array(free_pages_array, mbinfo->mmap_addr, mbinfo->mmap_length, startOfKernel_addr, endOfKernel_addr);
	
	/* Display free chunks pool entries. */
#if VERBOSEBOOT == 1
	print_page_pool(free_pages_array);
#endif
	
	/* Try and locate the ACPI data tables. */
	terminal_puts("Attempting to locate ACPI tables...");
	struct rsd_ptr *rsd_pointer = find_rsd_ptr(0x000E0000, 0x00100000);
	if ((uint32_t)rsd_pointer != 0) {
#if VERBOSEBOOT == 1
		terminal_puts("Done. \nFound RSD Pointer at 0x");
		terminal_put_hexdw((uint32_t)rsd_pointer);
		terminal_puts(". \nFound RSDT at 0x");
		struct rsd_table *rsdt = (struct rsd_table *)(rsd_pointer->rsdt_addr);
		terminal_put_hexdw((uint32_t)rsdt);
		terminal_puts(" containing 0x");
		terminal_put_hexdw(((rsdt->length)-36)/4);
		terminal_puts(" entries. \n");
	} else {
		terminal_puts("Done. \nFailed to locate RSD Pointer. \n");
	}
#else
		terminal_puts("Done.\n");
	} else {
		terminal_puts("Fail, Failed to find RSD Pointer.");
	}
#endif
	
	/* Discover and describe PCI devices. */
	terminal_puts("Detecting PCI Devices...");
	struct PCI_basic_device_info pci_device_list[256];
	uint32_t no_of_devices = PCI_enumerate_devices(pci_device_list, 256);
	if (no_of_devices != 0) {
		terminal_puts("Done. \n");
#if VERBOSEBOOT == 1
		PCI_dump_devices(pci_device_list, 256);
#endif
	} else {
		terminal_puts("Fail, no PCI devices found.\n");
	}
	
	/* Initialise the ATA Port I/O functions. */
	terminal_puts("Initialising IDE Controller...");
	switch (ATA_PIO_initialise()) {
		case 0:
			terminal_puts("Done.\n");
#if VERBOSEBOOT == 1
			terminal_puts("Primary Channel Ports: 0x");
			terminal_put_hexw(IDE_BASE_ADDR_PRIMARY);
			terminal_puts("/0x");
			terminal_put_hexw(IDE_CONTROL_PRIMARY);
			terminal_puts("\nSecondary Channel Ports: 0x");
			terminal_put_hexw(IDE_BASE_ADDR_SECONDARY);
			terminal_puts("/0x");
			terminal_put_hexw(IDE_CONTROL_SECONDARY);
			terminal_puts("\n");
			terminal_puts("Primary Master Device Type: 0x");
			terminal_put_hexb(IDE_drive_type[0]);
			terminal_puts(" Primary Slave Device Type: 0x");
			terminal_put_hexb(IDE_drive_type[1]);
			terminal_puts("\n");
			terminal_puts("Secondary Master Device Type: 0x");
			terminal_put_hexb(IDE_drive_type[2]);
			terminal_puts(" Secondary Slave Device Type: 0x");
			terminal_put_hexb(IDE_drive_type[3]);
			terminal_puts("\n");
#endif
			break;
		case 1:
			terminal_puts("Fail, IDE Controller not found.\n");
			break;
		case 2:
			terminal_puts("Fail, native PCI mode not supported on primary channel.\n");
			break;
		case 3:
			terminal_puts("Fail, native PCI mode not supported on secondary channel.\n");
			break;
		default:
			terminal_puts("Fail, unknown error.\n");
			break;
	}
	
	/* Initialise paging in the kernel. */
	terminal_puts("Initialising paging...");
	initialise_kernel_paging(startOfKernel_addr, endOfKernel_addr);
	terminal_puts("Done.\n");
	
#define DEBUG_PAGEFAULT 0
#if DEBUG_PAGEFAULT == 1
	/* Cause a pagefault to see if it gets handled. */
	terminal_puts("Invoking page fault...");
	volatile uint32_t pftest = *(uint32_t *)0xFF000000;
	terminal_puts("Done.\n");
#endif
	
	/* Try and read the partition table from the first hard drive. */
	terminal_puts("Reading Partition table from first ATA disk...");
	uint8_t drive_no = 5;
	uint16_t MBR_buff[257];
	uint32_t MBR_read_result;
	for (int i = 0; i < 4; i++) {
		if (IDE_drive_type[i] == 1) {
			drive_no = i;
			break;
		}
	}
	if (drive_no == 5) {
		terminal_puts("Fail, No ATA disk.\n");
	} else {
		terminal_puts("\nReading from disk no ");
		terminal_put_hexb(drive_no);
		MBR_read_result = ATA_PIO_read_48(drive_no, 0, 1, MBR_buff);
		switch (MBR_read_result) {
			case 0:
				/* Let's find that MBR! */
				if (MBR_buff[255] == 0xAA55) {
					/* It's an MBR!!! */
					terminal_puts("\nPartition table: \n");
					for (int part = 0; part < 4; part++) {
						terminal_puts("P");
						terminal_put_hexb(part);
						terminal_puts(": ID - 0x");
						terminal_put_hexb(MBR_buff[223 + (part*8) + 2] & 0x00FF);
						terminal_puts(" / Offset - 0x");
						terminal_put_hexw((uint32_t)(MBR_buff[223 + (part*8) + 4]) | (uint32_t)(MBR_buff[223 + (part*8) + 5]) << 16);
						terminal_puts(" / Bootable - ");
						if ((MBR_buff[223 + (part*8)] & 0x80) != 0) {
							terminal_puts("Y \n");
						} else {
							terminal_puts("N \n");
						}
					}
				} else {
					terminal_puts("\nNo MBR found in boot sector.");
				}
				break;
			case 256:
				/* Drive error :'( */
				terminal_puts("\nError reading boot sector.");
				break;
		}
		terminal_puts("Done\n");
	}
	
	/* Ready to go! */
	terminal_puts("Ready: ");
	while (1) {
		service_keyboard();
	};
}

/*
 * kernel_crash
 * Stops the kernel due to a critical unrecoverable error.
*/
void kernel_crash(char *msg) 
{
	terminal_set_color(make_color_data(COLOR_RED, COLOR_WHITE));
	terminal_set_pos(0, 0);
	terminal_puts("The kernel has encountered a critical error and must immediately halt. If there is a reason for this crash, it will be stated below. When you are ready, you    can restart your computer. The reason for the error is:                         "); 
	if (msg != NULL) {
		terminal_puts(msg);
	}
	terminal_set_pos(0, 24);
	terminal_puts("You can recover from this error by restarting your PC now.                      ");
	kernel_halt();
}

/* 
 * kernel_halt
 * Stops execution and disables interrupts, hanging the system until a restart
 * is carried out. Use when the user NEEDs to restart. 
*/
void kernel_halt() {
	__asm__("cli"); /* Clear interrupts so we can't escape the hang. */
	hang();
}
