; io.asm
; Daniel Selmes 2015
; Functions for using the input and output instructions on an x86 machine.
; These are designed to be used from C code. A C header provides a list of
; common port numbers and data items to use with these instructions. 

section .text

global inb, inw, inl, outb, outw, outl, io_wait

; Read a byte from a port. C declaration:
; uint8_t inb(uint16_t port);
inb:
	push ebp		; New Stack Frame
	mov ebp, esp		;
	mov eax, 0		; Zero out eax
	xor edx, 0		; Zero out edx
	mov dx, [ebp + 8]	; Put the port to read in dx
	in al, dx		; Read the byte into ax
	pop ebp			; Destroy stack frame
	ret

; Read a word from a port. C declaration:
; uint16_t inb(uint16_t port);
inw:
	push ebp		; New Stack Frame
	mov ebp, esp		;
	mov eax, 0		; Zero out eax
	xor edx, 0		; Zero out edx
	mov dx, [ebp + 8]	; Put the port to read in dx
	in ax, dx		; Read the byte into ax
	pop ebp			; Destroy stack frame
	ret
	
; Read a long from a port. C declaration:
; uint32_t inb(uint16_t port);
inl:
	push ebp		; New Stack Frame
	mov ebp, esp		;
	mov eax, 0		; Zero out eax
	xor edx, 0		; Zero out edx
	mov dx, [ebp + 8]	; Put the port to read in dx
	in eax, dx		; Read the byte into ax
	pop ebp			; Destroy stack frame
	ret

; Output a byte to a port. C declaration:
; void outb(uint16_t port, uint8_t data);
outb:
	push ebp		; New stack frame
	mov ebp, esp
	mov eax, 0		; Zero out eax
	mov edx, 0		; Zero out edx
	mov dx, [ebp + 8]	; Put the port to write in dx
	mov al, [ebp + 12]	; Put the data to write in al
	;xchg bx, bx		; BREAK HERE IN BOCHS
	out dx, al		; Perform the write
	pop ebp			; Destroy stack frame
	ret

; Output a word to a port. C declaration:
; void outw(uint16_t port, uint16_t data);
outw:
	push ebp		; New stack frame
	mov ebp, esp
	mov eax, 0		; Zero out eax
	mov edx, 0		; Zero out edx
	mov dx, [ebp + 8]	; Put the port to write in dx
	mov ax, [ebp + 12]	; Put the data to write in al
	out dx, ax		; Perform the write
	pop ebp			; Destroy stack frame
	ret

; Output a long to a port. C declaration:
; void outl(uint16_t port, uint32_t data);
outl:
	push ebp		; New stack frame
	mov ebp, esp
	mov eax, 0		; Zero out eax
	mov edx, 0		; Zero out edx
	mov dx, [ebp + 8]	; Put the port to write in dx
	mov eax, [ebp + 12]	; Put the data to write in al
	out dx, eax		; Perform the write
	pop ebp			; Destroy stack frame
	ret

; This function makes sure that IO operations have time to react on some
; older machines. 
io_wait:
	; JUST KIDDING. I'M NOT IMPLEMETING IT NOW SO IF YOU RUN INTO PROBLEMS CHECK HERE FIRST!
	ret