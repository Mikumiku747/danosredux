; idt_asm.asm
; Daniel Selmes 2016
; Assembly routines and data for creating, editing and loading the interrupt
; descriptor table. 

section .data

; Space for pointer to the interrupt descriptor table
idtp:	dd 0x07F8		; 0x8 * 0xFF
	dw 0x0

section .text
global idt_load, isr_handler_double_fault

; This loads an IDT into memory. Note that the length is fixed, so you MUST
; use an IDT with all 256 entries filled, even if most are "not present".
idt_load:
	mov eax, [esp + 4]	; Get the location of the idt
	mov [idtp + 2], eax	; Load the location into the pointer
	lidt [idtp]		; Actually load our IDT
	ret

; isr_handler_double_fault
; This is a surefire way to tell a double fault occurred. It loads the
; registers with obvious junk and then halts, meaning we can tell when
; we get a double fault and not some other kind of freeze. 
isr_handler_double_fault:
	mov eax, 0xDEADBEEF
	mov ebx, 0xDEADBEEF
	mov ecx, 0xCAFEBABE
	mov edx, 0xBEEFDEAD
	cli
	halt