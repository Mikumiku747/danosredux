; paging_asm.asm
; Daniel Selmes 2016
; Contains paging related assembly routines which perform functions tasks
; that can only be done in assembly. 

section .text

global install_paging_directory, set_paging_bit, isr_handler_page_fault
extern page_fault_handler

; install_page_directory
; Takes 1 parameter, the address of a page directory (4K aligned) to install
; in CR3. 
install_paging_directory:
	push ebp
	mov ebp, esp		; New stack frame

	mov eax, [ebp+8]		; Get Address of page directory
	mov cr3, eax		; Load the address into CR3

	mov esp, ebp
	pop ebp			; Get back old stack frame
	ret			; Return

; set_paging_bit
; Enables paging by setting bit 31 in the CR0 register. Takes no parameters. 
set_paging_bit:
	push ebp
	mov ebp, esp		; New stack frame

	mov eax, cr0		; Get the value of CR0
	or eax, 0x80000000	; Set bit 31
	mov cr0, eax		; ENABLES PAGING

	mov esp, ebp
	pop ebp			; Get back old stack frame
	ret			; Return

; isr_handler_page_fault
; Function wrapper for a page fault exception. 
isr_handler_page_fault:
	pushad			; Save register states
	pushfd			;
	
	mov eax, cr0 
	push eax		; Push the fault address as an argument.
	cld			; Clear the carry flag because C likes it
	call page_fault_handler	; Call our handler function
	add esp, 0x04		; Pop off the argument we gave. 
	 
	popfd			
	popad			; Restore CPU state
	add esp, 4		; Get the error code the CPU pushed off the stack. 
	iret
	