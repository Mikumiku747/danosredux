; CPUID_asm.asm
; Daniel Selmes 2016
; Assembly routines for discovering CPU features through the CPUID 
; instruction.

global test_for_CPUID, read_CPUID

section .text

; test_for_CPUID
; Uses the CPUID flag in eflags to test to see if the CPUID instruction is
; implemented and can be used. 
test_for_CPUID:
	pushfd 			; Save the flags register
	pushfd 			; Get a copy of the flags register we can mess around with
	xor dword [esp], 0x00200000	; Toggle the bit in the stored copy of eflags
	popfd			; Write it back to the eflags register
	pushfd			; Store another copy of eflags..
	pop eax			; And move it into eax
	xor eax, [esp]		; Will flip all bits in eax that are different from the saved eflags
	and eax, 0x00200000	; Makes sure that only the cpuid bit is set (if it was to begin with)
	shr eax, 21		; Shift the cpuid bit to 1 so we get either 0 or 1 as a result
	popfd			; Restore the original value of eflags
	ret			; Return
	
; read_CPUID
; Uses the CPUID instruction, first argument is the function to use (value of 
; eax to use with instruction), the next 4 arguments are places to store the
; registers when the CPUID function is done. 
read_CPUID:
	push ebp		; Make a new stack frame.
	mov ebp, esp
	
	push ebx
	push ecx
	push edx
	push esi		; Save the caller's registers
	
	mov eax, [ebp + 8]	; Load in the function code
	cpuid			; Perform the CPUID instruction
	
	mov esi, [ebp + 12]
	mov [esi], eax		; Save the output registers
	mov esi, [ebp + 16]
	mov [esi], ebx
	mov esi, [ebp + 20]
	mov [esi], ecx
	mov esi, [ebp + 24]
	mov [esi], edx
	
	pop esi  		; Restore the caller's registers
	pop edx
	pop ecx
	pop ebx
	
	mov esp, ebp		; Leave stack frame
	pop ebp
	ret


