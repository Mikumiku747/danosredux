; Bootstrap.asm
; Daniel Selmes 2016
; Sets up an environment for our kernel to load into.

; Set up constants for creating a multiboot header
MBALIGN		equ 0b00000001		; Flag to align loaded modules on page boundaries
MBINFO		equ 0b00000010		; Flag to provide us a memory map
SETGFX		equ 0b00000000		; Flag to attempt to set the video mode
FLAGS		equ MBALIGN | MBINFO | SETGFX	; Flags to be put in our header
MAGIC		equ 0x1BADB002		; Magic number let the bootloader find our header
CHECKSUM	equ -(MAGIC + FLAGS)	; Proves we are an actual multiboot header
GMODE		equ 0			; Graphics mode (0: Linear Bitmap, 1:Text Mode)
GWIDTH		equ 1024  		; Requested width (in pixels/chars, 0 for no preference)
GHEIGHT		equ 768  		; Requested height (in pixels/chars, 0 for no preference)
GDEPTH		equ 32			; Requested Color Depth (in bits, 0 for text mode or no preference)

; Declare our multiboot header. It's in its own section so we can force it to the front
section .multiboot
align 4
	dd MAGIC	; Header for multiboot kernel
	dd FLAGS	; Features requested by the OS
	dd CHECKSUM	; Checksum for first 12 bytes of header
	resb 20 	; Leave a gap for the non-present address fields of the header
	dd GMODE	; Requested graphics environment from kernel
	dd GWIDTH
	dd GHEIGHT
	dd GDEPTH
	
; Set up some space for our stack
section .bootstrap_stack, nobits
align 4
stack_bottom:	; Here is where the stack space starts
resb 16384	; Allocate 16 kiB
stack_top:	; The top of the stack, where we begin using it (The stack grows down...)

; Start our kernel bootstrap code
section .text
; Global names
global _start
global hang
; External names
extern kernel_main
_start:
	; Set up the stack so we can use it
	mov esp, stack_top	; The stack pointer is now valid.
	; Ebx contains multiboot info, so pass it to our kernel_main function.
	push ebx
	; Eax contains a magic value if we got loaded by a multiboot spec loader
	push eax
	; Clear interrupts, we'll put them back when we're ready for them.
	cli
	; Call our kernel_main and begin running the kernel!
	call kernel_main
	; Note that the kernel should NEVER return. in the case it does, we will hang the machine.
	cli		; Clear interrupts, so they can't let us out of our hang loop.
hang:	hlt		; Stop execution.
	jmp hang;	; Go back to hanging if we ever continue.
