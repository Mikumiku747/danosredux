; keyboard_asm.asm
; Daniel Selmes 2016
; Interrupt service handler and data structures for keyboard handling. 

; Declare some common data pieces like port numbers and commands
KBD_PORT_IO: equ 60h
KBD_PORT_CONTROL: equ 64h

section .text
global isr_handler_keyboard
extern keyboard_handler
; So yeah, I abandoned writing the handler in assembler and went for an
; Assembly wrapper to C
isr_handler_keyboard:
    pushad	; Save state
    pushfd
    cld		; C functions need cf cleared for some reason
    call keyboard_handler
    popfd	; Restore state
    popad
    iret	; Interrupt Return

; The process of handling a keyboard interrupt is as follows:
; - Read the byte from the keyboard. It will be a set 1 scancode. 
; - Put it in the keyboard events buffer. This is a FIFO buffer of stuff we
;   have gotten from the keyboard. The OS services that at its own pace. 
; - Tell the PIC we've handled the interrupt. 
; - Return to normal execution with the IRET instruction. 
; NOTE THAT I MIGHT DITCH THE ASM IMPLEMENTATION AND GO FOR A WRAPPER
; IF IT GETS TOO UNWIELDY (IT'S STARTING TO0)
; align 4
; isr_handler_keyboard:
; 	; We need to preserve all the registers...
; 	pushad
; 	pushfd
; 	; First, we read the data from the keyboard
; 	mov dx, KBD_PORT_IO
; 	in al, dx
; 	push al				; We'll put this on the stack and get it back later
; 	; Next, we need to store it in the queue. We first check that the queue is not full
; 	mov al, [scancode_buffer_in]	; Current input of the queue
; 	or ah, ah			; Clear out ah so that we only divide by al
; 	inc al 				; Get the next place in the queue
; 	mov cl, [scancode_buffer_size]	; We're going to perform a modulo of the queue position to make sure it wraps around
; 	div cl				; This will store the modulo of AX/CL in AH, and the quotient (likely 0) in AL
; 	mov al, ah			; We now adjusted the position of IN+1 so it is proper. 
; 	mov bl, [scancode_buffer_out]
; 	cmp al, bl
; 	je .byte_queue_full
; 	; We know there is space in the queue, so add it on to the queue.
; 	; But before we do, we need to check if it's a multibyte scancode. 
; 	pop bl				; We're going to leave AL alone, since it has the new value for in. 
; 	cmp bl, 0E0h			; A scancode of 0xE0 means it's extended
; 	je .multibyte_check		; In that case, go handle a multibyte input
; 	cmp bl, 0E1h			; It could also be the dreaded Pause!
; 	je .pause_write			; If so, we need to handle that monstrosity...
; 	mov cl, [scancode_buffer_in]	; Get the offset for the buffer's "in" slot. 
; 	mov byte [scancode_buffer_area + cl], bl ; We just add a single byte the "in" slot
; 	mov byte [scancode_buffer_in], al ; AL had the next free spot (with wrapping accounted for). 
; 	pop bl				; Since we didn't need to re-use it later, we can pop bl
; 	jmp .clear_pics			; Now we can move on and clear the PICs. 
; .multibyte_check:
; 	; First, we get the NEXT scancode, and see if there are any more bytes. 
; 	mov dx, KBD_PORT_IO
; 	in al, dx			; So we get the NEXT scancode
; 	cmp al, 2Ah			; If it's a prntscrn pressed...
; 	je .prntscrn_pressed		; go handle that specifically. 
; 	cmp al, 0B7h			; If it's a prntscrn released...
; 	je .prtnscrn_released		; then go handle that specifically. 
; 	push al				; Let's save the second half for later then...
; 	; So now, it must a be a 2-byte scancode. We need to make sure the second byte fits, so more modulo time
; 	mov al, [scancode_buffer_in]	; Current input of the queue
; 	xor ah, ah			; Clear out ah so that we only divide by al
; 	add al, 2 			; Get 2 places ahead in the queue
; 	mov cl, [scancode_buffer_size]	; We're going to perform a modulo of the queue position to make sure it wraps around
; 	div cl				; This will store the modulo of AX/CL in AH, and the quotient (likely 0) in AL
; 	mov al, ah			; We now adjusted the position of IN+1 so it is proper. 
; 	mov bl, [scancode_buffer_out]
; 	cmp al, bl
; 	je .word_queue_full		; If they are equal, we can't store two bytes, and thus have to toss the input
; 	; Now we know there's enough spots on the buffer for 2 bytes, so let's put 2 on. 
; 	pop dh				; Put the second byte in DH
; 	pop dl				; Put the first byte in DL
; 	; Put the first scancode byte in the buffer
; 	mov bl, [scancode_buffer_in]	; Get the offset of the buffer's "in" slot
; 	mov byte [scancode_buffer_area + bl], dl	; Put it in the offset given by bl
; 	; Keep the new "in" slot (current plus 2) for later. 
; 	push al
; 	; Find the "in" slot plus one again (where we put the second byte)
; 	mov al, [scancode_buffer_in]	; Current input of the queue
; 	or ah, ah			; Clear out ah so that we only divide by al
; 	inc al 				; Get the next place in the queue
; 	mov cl, [scancode_buffer_size]	; We're going to perform a modulo of the queue position to make sure it wraps around
; 	div cl				; This will store the modulo of AX/CL in AH, and the quotient (likely 0) in AL
; 	mov al, ah			; We now adjusted the position of IN+1 so it is proper. 
; 	; Put the second scancode byte in the buffer
; 	mov byte [scancode_buffer_area + al], dh	; Put it in the offset given by al ("in" + 1 wrapped)
; 	; Update the "in" slot to the new position. ( Old "in" slot plus 2)
; 	pop al				; Get the "plus 2" back
; 	mov byte [scancode_buffer_in], al ; Use it to update the "in" slot. 
; 	jmp .clear_pics
; .pause_write:
; 	; Nuthin' yet
; .prntscrn_pressed:
; 	; Nuthin' yet
; .prntscrn_released:
; 	; Nuthin' yet
; .word_queue_full:
; 	pop al				; Turns out, we couldn't store a 2 byte scancode, so pop the second byte off. 
; .byte_queue_full:			; In this case, the queue is full, so we have to drop the keyboard input
; 	pop al				; Get the first scancode we pushed off the stack.
; .clear_pics:				; Now we signal to the pics that 