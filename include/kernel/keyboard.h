/* 
 * keyboard.h
 * Daniel Selmes 2016
 * keyboard data structures and function delarations.
*/

/* Include protection. */
#ifndef KEYBOARD_H
#define KEYBOARD_H

/* Includes. */
#include <stdint.h>
#include <stddef.h>
#include <io.h>

#define PIC_EOI 0x20

/*
 * Here we define a bunch of commands and responses that we can send to the 
 * keyboard to configure and control it. A full reference of keyboard
 * commands can be found at http://www.computer-engineering.org/ps2keyboard/
*/

#define KB_COM_RESET 0xFF		/* Resets the keyboard. */
#define KB_COM_RESEND 0xFE		/* Resends last byte. */
#define KB_COM_DEFAULT 0xF6		/* Default keyboard settings. */
#define KB_COM_DISABLE 0xF5		/* Disables keyboard input. */
#define KB_COM_ENABLE 0xF4		/* Enables keyboard input. */
#define KB_COM_TYPEMATIC 0xF3		/* Configures typematic settings. */
#define KB_COM_READID 0xF2		/* Reads the keyboard ID. */
#define KB_COM_SCANCODE 0xF0		/* Gets or sets the scancode set. */
#define KB_COM_ECHO 0xEE		/* Echo command. */
#define KB_COM_SETLED 0xED		/* Sets LEDs on keayboard. */

/* The keyboard driver has a buffer of the scancodes it has received. This is
 * so that we can service the keyboard when we want to, as opposed to when the
 * keyboard wants us to. We use a FIFO buffer to keep track of the scancodes
 * that get read in. */

#define SCANCODE_BUFFER_SIZE 128

uint8_t scancode_buffer_area[SCANCODE_BUFFER_SIZE];
uint8_t scancode_buffer_in;
uint8_t scancode_buffer_out;

void scancode_buffer_init();

void keyboard_handler();

extern void isr_handler_keyboard();

#endif
