/* 
 * vga.h
 * Daniel Selmes 2016
 * Provides basic console functionality for VGA mode 3 screen output
*/

/* Include protection. */
#ifndef VGA_H
#define VGA_H

/* Includes */
#include <stdint.h>
#include <io.h>
#include <mem.h>

/* VGA Color Definitions. */
/* VGA Color Definitions. */
enum vga_color  {
	COLOR_BLACK = 0,
	COLOR_BLUE = 1,
	COLOR_GREEN = 2,
	COLOR_CYAN = 3,
	COLOR_RED = 4,
	COLOR_MAGENTA = 5,
	COLOR_BROWN = 6,
	COLOR_LIGHT_GREY = 7,
	COLOR_DARK_GREY = 8,
	COLOR_LIGHT_BLUE = 9,
	COLOR_LIGHT_GREEN = 10,
	COLOR_LIGHT_CYAN = 11,
	COLOR_LIGHT_RED = 12,
	COLOR_LIGHT_MAGENTA = 13,
	COLOR_LIGHT_BROWN = 14,
	COLOR_WHITE = 15,
};

/* Assembles color data for a VGA character */
uint8_t make_color_data(enum vga_color foreground, enum vga_color background);

/* Assembles a VGA character entry (with color data) */
uint16_t make_entry_data(char c, uint8_t colordata);

/* Terminal contstants and vars */
#define VGA_BUFFER_ADDRESS 0xB8000
#define ROWS 25
#define COLS 80
uint16_t terminal_row;
uint16_t terminal_col;
uint8_t terminal_color;
volatile uint16_t *terminal_buffer;

/*
 * Initialises the terminal variables and clears the screen buffer.
*/
void terminal_init();

/* Sets the terminal printing color */
void terminal_set_color(uint8_t color);

/* Sets the terminal position */
void terminal_set_pos(uint8_t col, uint8_t row);

/* Increment the character position by 1 */
void terminal_increment_pos();

/* Updates the cursor on the screen. */
void terminal_update_cursor();

/* Scroll the terminal up one line. */
void terminal_scroll();

/* Puts an entry on the screen */
void terminal_put_entry(uint16_t entry);

/* Puts an entry on the screen at the given point */
void terminal_put_entry_at(uint16_t entry, uint8_t col, uint8_t row);

/* Print a character on the screen. */
void terminal_putc(char c);

/* Print a character to the screen at the given position. */
void terminal_putc_at(char c, uint8_t col, uint8_t row);

/* Prints a string to the terminal. */
int terminal_puts(char *string);

/* Prints a 32-bit value to the terminal in hex form. */
void terminal_put_hexdw(uint32_t value);

/* Prints a 16-bit value to the terminal in hex form. */
void terminal_put_hexw(uint16_t value);

/* Prints a 8-bit value to the terminal in hex form. */
void terminal_put_hexb(uint8_t value);

/* Prints the hex value of an arbitrary size data type, you must provide a 
 * pointer and size in bytes of the object. Works only for up to maximum size 
 * integer supported. */
void terminal_put_hex_a(uintmax_t value, int size);

#endif