/* paging.h
 * Functions and structures related to the very hard details of paging, 
 * this is closely intertwined with the page_allocator. 
*/

/* Include protection. */
#ifndef PAGING_H
#define PAGING_H

/* Includes. */
#include <stdint.h>

/* Macros for the enable/disable of certain features in paging. */
#define PAGING_DIRECTORY_PRESENT 0x00000001
#define PAGING_DIRECTORY_WRITEABLE 0x00000002 
#define PAGING_DIRECTORY_USER_ACCESS 0x00000004
#define PAGING_DIRECTORY_WRITE_THROUGH 0x00000008
#define PAGING_DIRECTORY_DISABLE_CACHE 0x00000010
#define PAGING_DIRECTORY_ACCESSED 0x00000020
#define PAGING_DIRECTORY_LARGE_PAGES 0x00000080

#define PAGING_PAGE_PRESENT 0x00000001
#define PAGING_PAGE_WRITEABLE 0x00000002 
#define PAGING_PAGE_USER_ACCESS 0x00000004
#define PAGING_PAGE_WRITE_THROUGH 0x00000008
#define PAGING_PAGE_DISABLE_CACHE 0x00000010
#define PAGING_PAGE_READ 0x00000020
#define PAGING_PAGE_WRITTEN 0x00000040
#define PAGING_PAGE_GLOBAL 0x00000100


/* Page directory entry structure. */
struct page_directory_entry {
	unsigned int present : 1;
	unsigned int writeable : 1;
	unsigned int user_access : 1;
	unsigned int write_through : 1;
	unsigned int cache_disable : 1;
	unsigned int accessed : 1;
	unsigned int ignored : 1;
	unsigned int large_pages : 1;
	unsigned int unused : 4;
	unsigned int page_table_addr : 20;
} __attribute__ ((packed));

/* Page table entry structure. */
struct page_table_entry {
	unsigned int present : 1;
	unsigned int writeable : 1;
	unsigned int user_access : 1;
	unsigned int write_through : 1;
	unsigned int cache_disable : 1;
	unsigned int dirty_read : 1;
	unsigned int dirty_write : 1;
	unsigned int PAT : 1;
	unsigned int global : 1;
	unsigned int unused : 3;
	unsigned int page_table_addr : 20;
} __attribute__ ((packed));

/* Definitions of some page directories/tables which will certainly exist. */
/* The main page directory for when the main kernel code is running. */
struct page_directory_entry kernel_master_page_directory[1024] __attribute__ ((aligned (4096)));
/* The main page table which protects kernel code and memory mapped IO. */
struct page_table_entry kernel_reserved_page_table[1024] __attribute__ ((aligned (4096)));

/* Functions to get basic paging functionality running on the system. */
/* initialise_kernel_paging 
 * Initialises basic paging functionality by filling out some pre-allocated
 * page directories and tables, identity mapping the first 1MB and the main
 * kernel code, and enabling paging on the processor. You should install a 
 * very basic page fault interrupt handler before calling this, or you'll 
 * likely be getting triple faults. kernel_start and kernel_end are addresses 
 * for the start and end of the kernel. They should be page aligned, that is, 
 * aligned to the nearest 4k unit which covers the whole kernel. If the linker
 * is nice it should define them aligned, but in case it doesn't, this will 
 * check too. They also both need to be less than 4MB, or 0x00400000. Note
 * kernel_end should be the first 4K aligned address which the kernel is not
 * a part of. */
void initialise_kernel_paging(uint32_t kernel_start, uint32_t kernel_end);

/* page_fault_handler
 * Function which handles page fault exceptions, is passed some values by the 
 * CPU and an assembly wrapper. */
void page_fault_handler(uint32_t fault_address);

#endif