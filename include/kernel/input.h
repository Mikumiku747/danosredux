/*
 * input.h
 * Daniel Selmes 2016
 * Manages input for the operating system by servicing the scancode queue.
 * We also keep track of keyboard state...
*/

/* Include Protection. */
#ifndef INPUT_H
#define INPUT_H

/* Includes. */
#include <stdint.h>
#include <stddef.h>
#include <keyboard.h>
#include <vga.h>
#include <pic.h>

/* Some variables to keep track of common keyboard state settings. There
 * are also some macros to make easier use of them. */
#define KBD_SHIFT (kbd_shiftLeft || kbd_shiftRight) 
#define KBD_ALT (kbd_altLeft || kbd_altRight)
#define KBD_CTRL (kbd_ctrlLeft || kbd_ctrlRight)

/* This function causes the OS to service input. For now, it just prints 
 * characters to the screen so we can see the keyboard worked. In future, this
 * should feed the characters to the appropriate input feed of the current 
 * program, be it userspace or kernelspace. */
void service_keyboard();

#endif
