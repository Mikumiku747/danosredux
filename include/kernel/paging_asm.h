/* paging_asm.h
 * Daniel Selmes 2016
 * Provides assembly functions for doing paging stuff that can't be done in C,
 * such as loading CR3 and enabling the Paging bit is CR0. */

/* Include Protection. */
#ifndef PAGING_ASM_H
#define PAGING_ASM_H

/* Assembly functions for paging configuration. */
/* install_paging_directory
 * Assembly routine which installs the paging directory in CR3, 
 * page_directory_address is the ADDRESS of the page directory (It should be
 * 4K aligned, or bad things will happen. */
extern void install_paging_directory(uint32_t page_directory_address);

/* set_paging_bit
 * Sets bit 31 in CR0, which enables paging. You better be ready for this,
 * or you're going to crash and burn... */
extern void set_paging_bit();

/* isr_handler_page_fault
 * Interrupt service routine for page fault exception. */
extern void isr_handler_page_fault();

#endif
