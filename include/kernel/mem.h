/*
 * mem.h
 * Daniel Selmes 2016
 * Memory manipulation such as copying, clearing, and finding string length.
 * Note that these could probably be done in assembly to give a speed boost, 
 * and I might do that later.
*/

/* Include protection. */
#ifndef MEM_H
#define MEM_H

/* Includes */
#include <stdint.h>
#include <stddef.h>

/* Copy memory from one area to another. */
void* memcpy(void* restrict dstptr, const void* restrict srcptr, size_t size);

/* Fill a region of memory. */
void* memset(void* bufptr, int value, size_t size);

/* Compare two sections of memory. */
int memcmp(const void* aptr, const void* bptr, size_t size);

void* memmove(void* dstptr, const void* srcptr, size_t size);

#endif