/* 
 * idt.h
 * Daniel Selmes 2016
 * Structures and functions for creating, editing, and loading the interrupt
 * descriptor table (IDT). Includes C and Assembly routines. To use the C
 * routines, link against idt_c.o, and for the asm routines, idt_asm.o
*/

/* Include protection. */
#ifndef IDT_H
#define IDT_H

/* Includes. */
#include <stdint.h>
#include <stddef.h>

/* Here are a series of macros to set up the 'flags' section of IDT entries.
 * it combines a series of macros to allow you to easily set which options 
 * you want to enable in the flags field. */
#define IDT_F_PRESENT(x)	((x) << 7) /* Is the interrupt used? */
#define IDT_F_DPL(x)		((x) << 5) /* What level can call the entry */
#define IDT_F_STORAGE(x)	((x) << 4) /* Storage segment (0 for ints) */
#define IDT_F_TYPE(x)		((x) << 0) /* IDT entry type (0xE for ints).*/

/* We'll define some common flag fields for common entries in the IDT, like 
 * 32-bit interrupts. */
#define IDT_FLAGS_INT32		IDT_F_PRESENT(1) | IDT_F_DPL(0) | \
				IDT_F_STORAGE(0) | IDT_F_TYPE(0xE)
#define IDT_FLAGS_UNUSED	IDT_F_PRESENT(0) | IDT_F_DPL(0) | \
				IDT_F_STORAGE(0) | IDT_F_TYPE(0x0)

/* Here are some definitions of where common interrupts are: */
#define INTERRUPT_HARDWARE 0x20
#define INTERRUPT_KEYBOARD 0x01

#define INTERRUPT_EXCEPTION 0x00
#define INTERRUPT_DOUBLE_FAULT 0x08
#define INTERRUPT_PAGE_FAULT 0x0E

struct idt_entry {
	uint16_t offset_low;
	uint16_t selector;
	uint8_t reserved;
	uint8_t flags;
	uint16_t offset_high;
};

/* This fills out an IDT entry for you to make your life easier. */
void idt_encode_entry(int slot, void  (*offset)(), uint16_t selector, uint8_t flags);

/* This fills out a bunch of entries in the IDT for us. */
void idt_fill();

/* Here is the actual interrupt descriptor table data structure. It has 256 
 * 8-byte entries, one for each interrupt from 0x00 to 0xFF. */
struct idt_entry interrupt_descriptor_table[256];

/* Here are our Assembly routines and data items. */
extern int idt_load(struct idt_entry *idt);

extern void isr_handler_double_fault();

#endif