/*
 * memmap.h
 * Daniel Selmes 2016
 * Provides a map of available memory regions and thier size, and manipulates
 * them in preperation for the memory management system. 
*/

/* Include Protection. */
#ifndef MEMMAP_H
#define MEMMAP_H

/* Includes. */
#include <page_allocator.h>
#include <stdint.h>
#include <vga.h>

/* Memory map spec entry structure. Note that these entries are NOT 
 * CONSISTENT and can vary in lenght. You should look at the 4 bytes before
 * each entry to find the length of that entry (and hence the start of the 
 * next one). */
struct memmap_entry {
	uint64_t base_addr;
	uint64_t length;
	uint32_t type;
};

/* 
 * print_bios_memmap
 * Debug call to show the memory map the bootloader gave us. It pretty much
 * just does a GRUB 'lsmmap' command from inside the OS. We use this to help
 * debug when setting up the memory manager.
*/
void print_bios_memmap(uint32_t length, uint32_t start_addr);

/* Just prints out the raw bytes of the BIOS memory map, useful for taking
 * values off a real PC for a unit test of your memmap processing algorithm.*/
void print_bios_memmap_raw(uint32_t length, uint32_t start_addr);

/* initialise_free_chunks_array
 * Initialises the array of free chunks in memory. Takes the free chunk table
 * and adds in the available areas (less the kernel space) described by the 
 * grub memory map. */
void initialise_free_chunks_array(struct free_page_chunk free_page_array[], 
				  uint32_t memmap_start, 
				  uint32_t memmap_length, 
				  uint32_t kernel_start,
				  uint32_t kernel_end
				 );

/* Prints off details of all the slots of the free pages pool. */
void print_page_pool(uint32_t start_addr);

#endif
