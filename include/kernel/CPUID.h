/* CPUID.h
 * Daniel Selmes 2016
 * Functions and definitions for detecting processor features using the CPUID
 * instruction.
*/

/* Include Protection. */
#ifndef CPUID_H
#define CPUID_H

/* Includes. */
#include <stdint.h>

/* Assembly routines to return various values. */

/* test_for_CPUID
 * tests the eflags register to ensure that the CPUID instruction is
 * supported. Returns 0 for not supported, 1 for supported. */
extern uint32_t test_for_CPUID();

/* read_CPUID
 * Runs the CPUID instruction using the given command and puts the results 
 * of the registers in the supplied locations. Function is the CPUID function 
 * to use, the other arguments are pointers to places to store the registers.
 * Returns the value of EAX (since this is most often the desired value. */
extern uint32_t read_CPUID(uint32_t function, uint32_t *eax, uint32_t *ebx, 
		    uint32_t *ecx, uint32_t *edx);

/* C functions for processing the CPUID information. For most of these, you 
 * should check you can actually run the CPUID instruction first. */

/* dump_vendor_string
 * Prints the vendor string to the console. */
void dump_vendor_string();

/* dump_CPU_features. 
 * Prints the features the CPU supports to the console. */
void dump_CPU_features();

#endif