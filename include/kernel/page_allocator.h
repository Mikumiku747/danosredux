/*
 * page_allocator.h
 * Daniel Selmes 2016
 * Header for page-level memory management, including allocation, 
 * de-allocation, virtualisiation, and management structure info. 
*/

/* Include Protection */
#ifndef MANAGER_H
#define MANAGER_H

/* Includes. */
#include <stdint.h>
#include <stddef.h>

/* Free page chunk. */
struct free_page_chunk {
	uint32_t position;
	uint32_t length;
};

/* Some memory set aside for chunks of free pages. */
struct free_page_chunk free_pages_array[1024];

#endif