/* 
 * debug.h
 * Daniel Selmes 2016
 * These are some debug related macros stuff for analysing the state of the
 * machine during runtime. It's mostly for debugging the kernel during dev, 
 * and mostly just macros and shorthands. */

#ifndef DEBUG_H
#define DEBUG_H

/* This one enables bochs magic breakpoints! Note that you still need the 
 * semicolon on the end of this when using it. */
#ifndef DISABLE_BOCHS_BREAKPOINTS
#define MAGIC_BREAKPOINT __asm__("xchgw %bx, %bx")
#else
#define MAGIC_BREAKPOINT 
#endif


#endif