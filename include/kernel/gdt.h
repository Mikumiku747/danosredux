/* 
 * gdt.h
 * Daniel Selmes 2016
 * Header file for GDT assembly routines. 
*/

/* Include protection. */
#ifndef GDT_H
#define GDT_H

/* 
 * Loads our GDT so we can be certain of what segments are where now. We use
 * a flat memory model with the code data segments going from 0 to the top
 * of memory. This assembly routine is located in gdt.asm
*/
extern int loadGDT();

#endif
