/*
 * acpi.h
 * Daniel Selmes 2016
 * Provides headers for Advanced Configuration and Power Interface (ACPI)
*/

/* Include Protection */
#ifndef ACPI_H
#define ACPI_H

/* Includes */
#include <stdint.h>

/* Root System Description Pointer structure
 * This is the data structure used to find the rest of the ACPI data tables,
 * you need to locate it yourself by finding the signature and checksum. */

struct rsd_ptr {
	char signature[8]; /* This should always be "RSD PTR " */
	uint8_t checksum; /* Used to enforce/check the table's integrity. */
	char OEMID[6]; /* OEM ID String. */
	uint8_t revision; /* 0 for ACPI 1.0, 1+ for ACPI 2.0 onwards. */
	uint32_t rsdt_addr; /* Actual address of the RSDT. */
	
};

/* Extended System Description Pointer structure
 * Later versions of ACPI use a larger table with 64-bit pointers, and so this
 * pointer is for another seperate table that uses 64 bit pointers. */

struct xsdt_ptr {
	uint64_t signature; /* This should always be "RSD PTR " */
	uint8_t checksum; /* Used to enforce/check the table's integrity. */
	uint32_t OEMID_4;
	uint16_t OEMID_2; /* OEM ID String. */
	uint8_t revision; /* 0 for ACPI 1.0, 1+ for ACPI 2.0 onwards. */
	uint32_t rsdt_addr; /* Actual address of the RSDT. */
	uint32_t length; /* Length of the whole table. */
	uint64_t xsdt_addr; /* Actual address of the XSDT (64 bit). */
	uint8_t extended_checksum; /*Checksum for all bytes in xsdt_ptr. */
	uint8_t reserved[3]; /* Reserved data. */
};

/* rsd_table
 * Structure which contains pointers for the rest of the ACPI information
 * tables. Note that the table also includes a list of 32-bit pointers up 
 * to the length indicated in this part of the table a well. */

struct rsd_table {
	uint32_t signature; /* Should be "RSDT", or 0x54445352. */
	uint32_t length; /* Length in bytes of this table. */
	uint8_t revision; /* Should be 1 since this is ACPI 1 only. */
	uint8_t checksum; /* Table sum should be zero. */
	uint32_t OEMID_4;
	uint16_t OEMID_2; /* OEM Identification String */
	uint64_t OEM_TABLE_ID; /* OEM Table ID string. */
	uint32_t OEM_revision; /* OEM revision. */
	uint32_t creator_ID; /* See ACPI reference for details. */
	uint32_t creator_revision; /*		""		*/
};


/* find_rsd_ptr
 * Searches for the RSDT pointer structure in a given memory area. It looks
 * for the string "RSD PTR " on a 16 byte boundary, so you should supply it a 
 * 16 byte aligned address. Also validates any entries it finds to ensure they
 * pass at least the rsd_ptr checksum. */

struct rsd_ptr *find_rsd_ptr(uint32_t start, uint32_t end);

#endif
