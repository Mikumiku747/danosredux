/* 
 * io.h
 * Daniel Selmes 2016
 * Functions for input and output to the ports on an x86 machine. These
 * functions are implemented in assembly. You can use this header for common
 * and known port numbers as well, such as the PIC and PS/2 controller. 
*/

/* Include protection. */
#ifndef IO_H
#define IO_H

/* Here are the port numbers for the Programmable interrupt controllers 
 * (also called the PICs). */
#define PORT_PIC1 		0x20
#define PORT_PIC2 		0xA0
#define PORT_PIC1_COMMAND	PORT_PIC1
#define PORT_PIC1_DATA 		(PORT_PIC1 + 1)
#define PORT_PIC2_COMMAND	PORT_PIC2
#define PORT_PIC2_DATA 		(PORT_PIC2 + 1)

/* Here we declare some ports used by the VGA Harware. These are mostly used 
 * by vga.c to adjust the cursor position. */
#define PORT_VGA_3D4		0x3D4
#define PORT_VGA_3D4_INDEX	PORT_VGA_3D4
#define PORT_VGA_3D4_RW		(PORT_VGA_3D4+1)

/* Here we declare the ports that the keyboard uses to send and receive 
 * commands and data. They're actually hooked up to the 8084, not the keyboard
 * itself, but whatever, hardware details... */
#define PORT_KBD_IO 0x60
#define PORT_KBD_CONTROL 0x64

/* Here we declare the prototypes for the inx and outx functions. They are
 * implemented in assembly in the io.asm source file. */
extern uint8_t inb(uint16_t port);
extern uint16_t inw(uint16_t port);
extern uint32_t inl(uint16_t port);
extern void outb(uint16_t port, uint8_t data);
extern void outw(uint16_t port, uint16_t data);
extern void outl(uint16_t port, uint32_t data);
/* There's also io_wait, which gives old pcs time to handle port 
 * interaction. */
extern void io_wait();

#endif
