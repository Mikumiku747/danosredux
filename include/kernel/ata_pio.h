/*
 * ata_pio.h
 * Daniel Selmes 2016
 * Headers for ATA Port I/O related functions, used to read and write to 
 * disks early on in system life. */

/* Include Protection */
#ifndef ATA_PIO_H
#define ATA_PIO_H

/* Includes */
#include <stdint.h>

/* Global Data Values for ATA PIO. */
/* The Ports, which are discovered during boot. */
uint16_t IDE_BASE_ADDR_PRIMARY;
uint16_t IDE_CONTROL_PRIMARY;
uint16_t IDE_BASE_ADDR_SECONDARY;
uint16_t IDE_CONTROL_SECONDARY; 

/* Macros for all the ports. */
#define IDE_PRIMARY_DATA IDE_BASE_ADDR_PRIMARY + 0x00
#define IDE_PRIMARY_FEATURES IDE_BASE_ADDR_PRIMARY + 0x01
#define IDE_PRIMARY_ERROR IDE_BASE_ADDR_PRIMARY + 0x01
#define IDE_PRIMARY_SECTOR_COUNT IDE_BASE_ADDR_PRIMARY + 0x02
#define IDE_PRIMARY_LBALOW IDE_BASE_ADDR_PRIMARY + 0x03
#define IDE_PRIMARY_LBAMID IDE_BASE_ADDR_PRIMARY + 0x04
#define IDE_PRIMARY_LBAHIGH IDE_BASE_ADDR_PRIMARY + 0x05
#define IDE_PRIMARY_CYLNDER_LOW IDE_BASE_ADDR_PRIMARY + 0x04
#define IDE_PRIMARY_CYLNDER_HIGH IDE_BASE_ADDR_PRIMARY + 0x05
#define IDE_PRIMARY_DRIVESEL IDE_BASE_ADDR_PRIMARY + 0x06
#define IDE_PRIMARY_HEAD IDE_BASE_ADDR_PRIMARY + 0x06
#define IDE_PRIMARY_COMMAND IDE_BASE_ADDR_PRIMARY + 0x07
#define IDE_PRIMARY_STATUS IDE_BASE_ADDR_PRIMARY + 0x07
#define IDE_PRIMARY_CONTROL IDE_CONTROL_PRIMARY
#define IDE_PRIMARY_ALT_STATUS IDE_CONTROL_PRIMARY
#define IDE_SECONDARY_DATA IDE_BASE_ADDR_SECONDARY + 0x00
#define IDE_SECONDARY_FEATURES IDE_BASE_ADDR_SECONDARY + 0x01
#define IDE_SECONDARY_ERROR IDE_BASE_ADDR_SECONDARY + 0x01
#define IDE_SECONDARY_SECTOR_COUNT IDE_BASE_ADDR_SECONDARY + 0x02
#define IDE_SECONDARY_LBALOW IDE_BASE_ADDR_SECONDARY + 0x03
#define IDE_SECONDARY_LBAMID IDE_BASE_ADDR_SECONDARY + 0x04
#define IDE_SECONDARY_LBAHIGH IDE_BASE_ADDR_SECONDARY + 0x05
#define IDE_SECONDARY_CYLNDER_LOW IDE_BASE_ADDR_SECONDARY + 0x04
#define IDE_SECONDARY_CYLNDER_HIGH IDE_BASE_ADDR_SECONDARY + 0x05
#define IDE_SECONDARY_DRIVESEL IDE_BASE_ADDR_SECONDARY + 0x06
#define IDE_SECONDARY_HEAD IDE_BASE_ADDR_SECONDARY + 0x06
#define IDE_SECONDARY_COMMAND IDE_BASE_ADDR_SECONDARY + 0x07
#define IDE_SECONDARY_STATUS IDE_BASE_ADDR_SECONDARY + 0x07
#define IDE_SECONDARY_CONTROL IDE_CONTROL_SECONDARY
#define IDE_SECONDARY_ALT_STATUS IDE_CONTROL_SECONDARY

/* Macros for some commands and other special values. */
#define IDE_COMMAND_IDENTIFY 0xEC
#define IDE_COMMAND_READ_SECTORS 0x20
#define IDE_COMMAND_WRITE_SECTORS 0x30
#define IDE_COMMAND_READ_SECTORS_EXT 0x24
#define IDE_COMMAND_WRITE_SECTORS_EXT 0x34

/* Some data structures for keeping track of drive state. */
/* Keeps track of the drive type (and presence) for the IDE controller. */
uint8_t IDE_drive_type[4];
/* Used to read IDE drive state into from an IDENTIFY command. */
struct IDE_identify_result {
	uint16_t data_shorts[256];
};
struct IDE_identify_result_ATA_formatted {
	uint16_t other0[82];
	uint16_t supported_commands_1;
	uint16_t supported_commands_2;
	uint16_t supported_commands_ext;
	uint16_t enabled_commands_1;
	uint16_t enables_commands_2;
	uint16_t default_commands_1;
	uint16_t default_commands_2; 
	uint16_t other1[11];
	uint16_t lba_48_max_low;
	uint16_t lba_48_max_mid;
	uint16_t lba_48_max_high;
	uint16_t other[155];
};
uint16_t identify_results[4][256];

/* ATA_PIO_initialise
 * Initialises ATA PIO related data, including locating and detecting drives
 * and preparing them for use. Returns 0 on success, Anything else is an 
 * error. */
uint32_t ATA_PIO_initialise();

/* ATA_PIO_read_48
 * Reads a set of sectors into a buffer. Can be physical or virtual. Drive is
 * the drive number to use, must be from 0 to 3 (see drive macros). LBA_48 is
 * the linear block address of the address to be read. Sectors is the number
 * of sectors to be read. Buffer is the buffer for the data to be read into,
 * please ensure that it is large enough to handle the number of sectors
 * requested. Returns 0 on success or an error code: 
 * 1 - Drive Error, nonexistent or non ATA drive. 
 * 2 - Feature error, drive can't do this command. 
 * 3 - LBA error, invalid LBA (over 28 bits or too big for drive)
 * 256 - Other error. 
*/
uint32_t ATA_PIO_read_48(uint8_t drive, uint64_t  LBA_48, uint32_t sectors, void *buffer);

/* ATA_PIO_read_28
 * Reads a set of sectors into a buffer, provided by a virtual address. drive
 * is the drive to use (see the drive macros), LBA_28 is a 28 bit LBA to start
 * reading from, sectors is the number of sectors to read, and buffer is the
 * buffer to write the data into. Returns 0 on success or an error code: 
 * 1 - Drive Error, nonexistent or non ATA drive. 
 * 2 - Feature error, drive can't do this command. 
 * 3 - LBA error, invalid LBA (over 28 bits or too big for drive)
 * 256 - Other error. 
*/
uint32_t ATA_PIO_read_28(uint8_t drive, uint32_t LBA_28, uint16_t sectors, void *buffer);

#endif