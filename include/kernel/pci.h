/*
 * pci.h
 * Daniel Selmes 2016
 * Provides headers related to PCI functions which search for and interact
 * with PCI devices and the PCI configuration space.
*/

/* Include Protection */
#ifndef PCI_H
#define PCI_H

/* Includes */
#include <stdint.h>

/* Constants */
#define PCI_CONF_ADDR 0xCF8
#define PCI_CONF_DATA 0xCFC
/* The maximum number of devices to keep in the global device list. */
#define PCI_MAX_GLOBAL_DEVICES 256

/* Functions for PCI configuration space interaction. */

/* PCI_conf_set_addr
 * Uses the PCI address port to set the configuration space address to be
 * read. Use in conjunction with PCI_conf_read and PCI_conf_write. */
void PCI_conf_set_addr(uint8_t bus, uint8_t device, uint8_t function, uint8_t reg);

/* PCI_conf_get_addr
 * Use this to get the current PCI configuration space register, you'll need
 * to decode it yourself. */
uint32_t PCI_conf_get_addr();

/* PCI_conf_read
 * Reads a word from PCI configuration space, use PCI_conf_set_addr to set
 * which PCI configuration register to read from. */
uint32_t PCI_conf_read();

/* PCI_conf_write
 * writes a word to PCI configuration space, use PCI_conf_set_addr to set 
 * which PCI configuration register to write to. */
void PCI_conf_write(uint32_t data);

/* PCI_conf_read_data
 * Reads out data from a specific PCI configuration register, by first setting
 * the register to be read from and then performing a read. */
uint32_t PCI_conf_read_data(uint8_t bus, uint8_t device, uint8_t function, uint8_t reg);

/* PCI_conf_write_data
 * Writes out data to a specific PCI configuration register by first setting
 * the register to be written to and then performing a write. */
void PCI_conf_write_data(uint8_t bus, uint8_t device, uint8_t function, uint8_t reg, uint32_t data);

/* PCI_basic_device-info
 * A structure for storing important information about a PCI device, such as
 * its ID numbers and class. */
struct PCI_basic_device_info {
	uint8_t bus;
	uint8_t device;
	uint8_t function;
	uint16_t vendor_ID;
	uint16_t device_ID;
	uint8_t class;
	uint8_t subclass;  
};

/* PCI_enumerate_devices
* Searches through the PCI Configuration space for all devices which exist. 
* device_list is an array of device info structures to put the found devices
* in. limit is the maximum amount of devices to put into the array, after 
* which function will return.  A device is considered valid if its vendor ID
* is not 0xFFFF. Returns number of devices found. */
uint32_t PCI_enumerate_devices(struct PCI_basic_device_info *device_list, uint32_t limit);

/* PCI_dump_devices
 * Dumps a list of the PCI devices currently on the PCI bus. */
void PCI_dump_devices(struct PCI_basic_device_info *pci_device_list, uint32_t limit);

/* PCI_global_device_list and PCI_global_device_count
 * A list of all the devices on the PCI bus, so that it doesn't need to be
 * searched by every single thing that uses PCI. Use PCI_global_enumerate
 * to update it. PCI_global_device_count contains the number of devices in 
 * the list. */
struct PCI_basic_device_info PCI_global_device_list[PCI_MAX_GLOBAL_DEVICES];
uint32_t PCI_global_device_count; 

/* PCI_global_enumerate
 * Enumerates the global device list, and notes the number of devices in the 
 * list as well. */
void PCI_global_enumerate();


#endif
