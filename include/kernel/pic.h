/* 
 * pic.h
 * Daniel Selmes 2016
 * Functions for controlling and configuring the 8259A Programmable interrupt
 * controller (PIC).
*/

/* Include protection. */
#ifndef PIC_H
#define PIC_H

/* Includes. */
#include <stddef.h>
#include <stdint.h>
#include <io.h>

/* Here we detail various command bytes for the PIC. */
#define PIC_INIT 0x10		/* Initialse command. */
#define PIC_EOI 0x20		/* End of interrupt command. */

/* Here we define the new offsets for our PICs. We'll put them just after the
 * 32bit exception interrupts, at 0x20-0x27 for the master and 0x28-0x2f for 
 * the slave. */
#define PIC1_OFFSET 0x20
#define PIC2_OFFSET 0x28

/* We define a mode for the PIC to operate in. For new, we just use the 8086
 * mode. We also define some constants for configuring the PIC. */
#define PIC_MODE_8086 0x01
#define PIC_INIT_MODE 0x01

/* This will re-map the PICS to thier new offsets. Also masks all, so we don't
 * get any unwelcome interrupts until we're ready for them. */
void PIC_remap();

/* These allow us to mask and unmask a line on the PICs when we want to use or
 * disable that line. Masking disables, unmasking enables. */
void PIC_mask_line(int line);
void PIC_unmask_line(int line);


#endif